package com.alexandershtanko.whereareyou.ui.adapter;

import android.content.Context;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.alexandershtanko.whereareyou.R;
import com.alexandershtanko.whereareyou.model.Contact;
import com.alexandershtanko.whereareyou.model.SmsMessage;
import com.alexandershtanko.whereareyou.util.ContactUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by alexander on 6/16/15.
 */
public class HistoryAdapter extends BaseAdapter implements SwipeRefreshLayout.OnRefreshListener {
    private static final int LAYOUT_ITEM_REQUEST_RES = R.layout.listview_item_request_historylist;
    private static final int LAYOUT_ITEM_RESPONSE_RES = R.layout.listview_item_response_historylist;
    private final SwipeRefreshLayout srLayout;
    private HashMap<String, Contact> contactHashMap = new HashMap<>();
    private Context context;
    private boolean flgSelection = false;
    private String filterText;

    private List<SmsMessage> selectedSmsMessageList = new ArrayList<>();

    private List<SmsMessage> smsMessageList;
    private List<SmsMessage> filteredSmsMessageList = Collections.EMPTY_LIST;


    public HistoryAdapter(Context context, List<SmsMessage> smsMessageList, SwipeRefreshLayout srLayout) {
        this.context = context;
        this.srLayout = srLayout;
        this.smsMessageList = smsMessageList;

        srLayout.setOnRefreshListener(this);
        srLayout.setEnabled(false);
        updateContactCache();

        filter(null);
    }


    @Override
    public int getCount() {
        return filteredSmsMessageList.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredSmsMessageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        SmsMessage smsMessage = filteredSmsMessageList.get(position);
        if (view == null) {
            view = inflateItemLayout(smsMessage.messageType);
        }
        ViewHolder holder = getViewHolder(view);

        Contact contact = getContactByPhone(smsMessage.phone);

        if (contact.photoUri != null) {
            holder.ivPhoto.setImageURI(contact.photoUri);
            holder.ivPhoto.setVisibility(View.VISIBLE);
            holder.tvLetter.setVisibility(View.GONE);
        } else {
            if (contact.name != null && contact.name.length() > 0)
                holder.tvLetter.setText((contact.name.charAt(0) + "").toUpperCase());
            holder.tvLetter.setVisibility(View.VISIBLE);
            holder.ivPhoto.setVisibility(View.GONE);
        }


        holder.tvName.setText(contact.name);
        holder.tvMessage.setText(smsMessage.message);
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yy  HH:mm");
        holder.tvDate.setText(df.format(new Date(smsMessage.dateAdd)));

        if (flgSelection)
            if (selectedSmsMessageList.contains(smsMessage))
                holder.cbSelect.setChecked(true);
            else
                holder.cbSelect.setChecked(false);

        holder.cbSelect.setVisibility(flgSelection ? View.VISIBLE : View.GONE);


        return view;
    }

    public void filter(String text) {
        filterText = text;


        List<SmsMessage> list = new ArrayList<>();
        for (SmsMessage smsMessage : smsMessageList) {
            if (text != null && contactHashMap.containsKey(smsMessage.phone)) {
                Contact contact = contactHashMap.get(smsMessage.phone);
                if (contact.name != null &&
                        contact.name.toLowerCase().contains(text.toLowerCase().trim()) ||
                        smsMessage.phone.toLowerCase().contains(text.toLowerCase().trim())) {
                    list.add(smsMessage);
                }
            } else {
                if (text == null) {
                    list.add(smsMessage);
                }
            }

        }
        filteredSmsMessageList = list;
        notifyDataSetChanged();
    }

    private synchronized Contact getContactByPhone(String id) {
        if (contactHashMap.containsKey(id))
            return contactHashMap.get(id);
        else {
            return new Contact();
        }
    }


    private View inflateItemLayout(SmsMessage.MessageType messageType) {
        return LayoutInflater.from(context).inflate(((messageType == SmsMessage.MessageType.MESSAGE_REQUEST) ? LAYOUT_ITEM_REQUEST_RES : LAYOUT_ITEM_RESPONSE_RES), null);
    }

    private ViewHolder getViewHolder(View view) {
        ViewHolder holder;
        if (view.getTag() == null) {
            holder = new ViewHolder();
            holder.tvName = (TextView) view.findViewById(R.id.tvName);
            holder.ivPhoto = (ImageView) view.findViewById(R.id.ivPhoto);
            holder.cbSelect = (CheckBox) view.findViewById(R.id.cbSelect);
            holder.tvDate = (TextView) view.findViewById(R.id.tvDate);
            holder.tvMessage = (TextView) view.findViewById(R.id.tvMessage);
            holder.tvLetter = (TextView) view.findViewById(R.id.tvLetter);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        return holder;
    }

    public void clearSelection() {
        selectedSmsMessageList.clear();
    }

    public void showSelection() {
        flgSelection = true;
    }

    public void hideSelection() {
        flgSelection = false;
    }

    public void switchCheckBox(SmsMessage smsMessage) {
        if (!selectedSmsMessageList.contains(smsMessage))
            selectedSmsMessageList.add(smsMessage);
        else
            selectedSmsMessageList.remove(smsMessage);
    }

    public boolean isSelectionMode() {
        return flgSelection;
    }


    public void updateContactCache() {
        (new AsyncUpdateCache()).execute();
    }

    @Override
    public void onRefresh() {
        updateContactCache();
    }

    public List<SmsMessage> getSelectedItems() {
        return selectedSmsMessageList;
    }

    @Override
    public int getItemViewType(int position) {
        SmsMessage smsMessage = filteredSmsMessageList.get(position);
        return (smsMessage.messageType == SmsMessage.MessageType.MESSAGE_REQUEST ? 0 : 1);
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }


    private static class ViewHolder {
        ImageView ivPhoto;
        TextView tvName;
        TextView tvDate;
        TextView tvMessage;
        CheckBox cbSelect;
        TextView tvLetter;
    }

    private class AsyncUpdateCache extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {

            if (smsMessageList != null)
                for (SmsMessage smsMessage : smsMessageList) {
                    if (!contactHashMap.containsKey(smsMessage.phone)) {
                        Contact contact = ContactUtils.getContactByPhone(context.getContentResolver(), smsMessage.phone);
                        try {
                            if (MediaStore.Images.Media.getBitmap(context.getContentResolver(), contact.photoUri) == null)
                                contact.photoUri = null;
                        } catch (Exception e) {
                            if(contact!=null)
                            contact.photoUri = null;
                        }
                        if(contact!=null)
                        contactHashMap.put(smsMessage.phone, contact);
                    }
                }
            return null;
        }

        @Override
        protected void onPreExecute() {
            TypedValue typed_value = new TypedValue();
            context.getTheme().resolveAttribute(android.support.v7.appcompat.R.attr.actionBarSize, typed_value, true);
            srLayout.setProgressViewOffset(false, 0, context.getResources().getDimensionPixelSize(typed_value.resourceId));
            srLayout.setRefreshing(true);
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            srLayout.setRefreshing(false);
            filter(filterText);
            super.onPostExecute(aVoid);
        }
    }

}
