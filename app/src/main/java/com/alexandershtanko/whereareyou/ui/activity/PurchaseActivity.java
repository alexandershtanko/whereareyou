package com.alexandershtanko.whereareyou.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.alexandershtanko.whereareyou.R;
import com.alexandershtanko.whereareyou.billing.PurchaseHelper;
import com.alexandershtanko.whereareyou.model.PurchaseConstants;
import com.alexandershtanko.whereareyou.ui.Settings;
import com.alexandershtanko.whereareyou.util.billing.Purchase;

public class PurchaseActivity extends AppCompatActivity {
    private static final String TAG = "PurchaseActivity";
    PurchaseHelper purchaseHelper;
    private PurchaseHelper.OnConsumeResultListener onConsumeResultListener=new PurchaseHelper.OnConsumeResultListener() {
        @Override
        public void onSuccess(Purchase info) {
            Settings.setPurchased(PurchaseActivity.this, true);
            Toast.makeText(PurchaseActivity.this, R.string.purchase_successed, Toast.LENGTH_LONG).show();
            finish();
        }

        @Override
        public void onError(String msg) {
            Toast.makeText(PurchaseActivity.this, R.string.purchase_failed, Toast.LENGTH_LONG).show();
            Log.e(TAG, msg);
            finish();
        }
    };

    private PurchaseHelper.OnPurchaseFinishedListener onPurchaseFinishedListener = new PurchaseHelper.OnPurchaseFinishedListener() {
        @Override
        public void onSuccess(Purchase info) {
            purchaseHelper.consumeItem(PurchaseConstants.SKU, onConsumeResultListener);
        }

        @Override
        public void onError(Purchase info, String msg) {
            Toast.makeText(PurchaseActivity.this, R.string.purchase_failed, Toast.LENGTH_LONG).show();
            Log.e(TAG, msg);
            finish();

        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!purchaseHelper.handleActivityResult(requestCode, resultCode, data))
            super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase);
        purchaseHelper = new PurchaseHelper(this);
        purchaseHelper.init(new PurchaseHelper.OnInitListener() {
            @Override
            public void onSuccess() {
                purchaseHelper.purchaseItem(PurchaseActivity.this, PurchaseConstants.SKU, PurchaseConstants.PURCHASE_TOKEN, onPurchaseFinishedListener);
            }

            @Override
            public void onError(String msg) {
                Toast.makeText(PurchaseActivity.this, R.string.unable_to_init_purchase, Toast.LENGTH_LONG).show();
                Log.e(TAG, msg);
                finish();
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        purchaseHelper.release();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_purchase, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
