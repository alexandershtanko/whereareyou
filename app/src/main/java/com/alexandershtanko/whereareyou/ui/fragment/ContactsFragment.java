package com.alexandershtanko.whereareyou.ui.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.alexandershtanko.whereareyou.R;
import com.alexandershtanko.whereareyou.ui.Settings;
import com.alexandershtanko.whereareyou.ui.activity.MainActivity;
import com.alexandershtanko.whereareyou.ui.adapter.ContactsAdapter;
import com.alexandershtanko.whereareyou.util.ContactUtils;
import com.alexandershtanko.whereareyou.util.PrefsUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ContactsFragment extends Fragment implements View.OnClickListener, BackPressed {


    private static final int REQUEST_CODE_PICK_CONTACT = 100;
    private static final String TAG = "ContactsFragment";
    private FloatingActionButton bAdd;
    private MainActivity activity;
    private ListView lvContacts;
    private ContactsAdapter contactsAdapter;

    private List<String> contactIds;
    private EditText etFilter;
    private SwipeRefreshLayout srLayout;
    private View lFocus;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_contacts, container, false);
        initView(view);
        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_contacts, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (contactsAdapter.isSelectionMode()) {
            menu.findItem(R.id.action_remove).setVisible(true);
            menu.findItem(R.id.action_edit).setVisible(false);
        } else {
            menu.findItem(R.id.action_remove).setVisible(false);
            menu.findItem(R.id.action_edit).setVisible(true);
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_remove:
                removeContactsFromContactList();
                break;
            case R.id.action_edit:
                showSelection();

        }
        return super.onOptionsItemSelected(item);
    }


    private Set<String> getContactIds() {
        Set<String> contactIds;
        try {
            contactIds = Settings.getContactIds(activity);
        } catch (PrefsUtils.PreferenceException e) {
            Log.e(TAG, "", e);
            contactIds = new HashSet<>();
        }
        return contactIds;
    }

    private void setContactIds(Set<String> contactIds) {
        try {
            Settings.setContactIds(activity, contactIds);
        } catch (PrefsUtils.PreferenceException e) {
            Log.e(TAG, "", e);
        }
    }


    private void initView(View view) {
        contactIds = new ArrayList<>(getContactIds());
        srLayout = (SwipeRefreshLayout) view.findViewById(R.id.srLayout);
        bAdd = (FloatingActionButton) view.findViewById(R.id.bAdd);
        bAdd.setOnClickListener(this);
        lvContacts = (ListView) view.findViewById(R.id.lvContacts);
        contactsAdapter = new ContactsAdapter(activity, contactIds, srLayout);
        lvContacts.setAdapter(contactsAdapter);
        lvContacts.setItemsCanFocus(true);
        lvContacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String contactId = contactsAdapter.getFilteredIds().get(position);
                if (!contactsAdapter.isSelectionMode()) {
                    openContactCard(contactId);
                } else {
                    contactsAdapter.switchCheckBox(contactId);
                    contactsAdapter.notifyDataSetChanged();
                }
            }
        });

        lvContacts.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                showSelection();
                return true;
            }
        });

        etFilter = (EditText) view.findViewById(R.id.tvFilter);
        etFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                contactsAdapter.filter(etFilter.getText().toString());
                contactsAdapter.notifyDataSetChanged();
            }
        });
        lFocus = view.findViewById(R.id.lFocus);
        hideSoftKeyboard(activity, etFilter);

    }

    private void hideSoftKeyboard(Activity activity, View view) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
        view.clearFocus();
        lFocus.requestFocus();
    }

    private void openContactCard(String contactId) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, String.valueOf(contactId));
        intent.setData(uri);
        activity.startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bAdd:

                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
                startActivityForResult(intent, REQUEST_CODE_PICK_CONTACT);
                break;
        }
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode) {
            case (REQUEST_CODE_PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    clearFilter();
                    addContactToList(ContactUtils.getContactId(activity.getContentResolver(), data));
                }
                break;
        }
    }

    private void addContactToList(String id) {
        if (id != null) {
            if (!contactIds.contains(id)) {
                contactIds.add(id);
                contactsAdapter.notifyDataSetChanged();
                contactsAdapter.updateContactCache();

                setContactIds(new HashSet(contactIds));

                Toast.makeText(activity, R.string.contact_added, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(activity, R.string.contact_already_in_list, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void removeContactsFromContactList() {
        List<String> selectedIds = contactsAdapter.getSelectedIds();
        for (String id : selectedIds) {
            contactIds.remove(id);
        }
        setContactIds(new HashSet(contactIds));
        contactsAdapter.updateContactCache();
        hideSelection();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MainActivity) getActivity();
        setHasOptionsMenu(true);
    }
    @Override
    public void onResume() {
        super.onResume();
        activity.setActionBarTitle(R.string.allowed_contacts);
    }

    private boolean isSelectionMode() {
        return contactsAdapter.isSelectionMode();
    }

    public boolean onBackPressed() {
        try {
            if (etFilter.getText().length() > 0 || etFilter.isFocused()) {
                clearFilter();
                return true;
            }

            if (isSelectionMode()) {
                hideSelection();
                return true;
            }
        }
        catch (Exception e)
        {
            Log.e(TAG,"",e);
        }
        return false;

    }

    private void clearFilter() {
        etFilter.setText("");
        hideSoftKeyboard(activity, etFilter);
    }

    private void hideSelection() {
        contactsAdapter.clearSelection();
        contactsAdapter.hideSelection();
        contactsAdapter.notifyDataSetChanged();
        activity.invalidateOptionsMenu();
        bAdd.setVisibility(View.VISIBLE);
    }

    private void showSelection() {
        contactsAdapter.showSelection();
        contactsAdapter.notifyDataSetChanged();
        activity.invalidateOptionsMenu();
        bAdd.setVisibility(View.GONE);

    }

    public static Fragment newInstance() {
        return new ContactsFragment();
    }
}
