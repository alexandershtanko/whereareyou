package com.alexandershtanko.whereareyou.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.alexandershtanko.whereareyou.R;
import com.alexandershtanko.whereareyou.model.Place;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by alexander on 6/16/15.
 */
public class PlacesAdapter extends BaseAdapter {
    private static final int LAYOUT_ITEM_RES = R.layout.listview_item_places;

    private List<Place> items = Collections.EMPTY_LIST;
    private Context context;
    private List<Place> selectedItems = new ArrayList<>();
    private boolean flgSelection = false;
    private List<Place> filteredItems = new ArrayList<>();
    private String filterText;


    public PlacesAdapter(Context context, List<Place> items) {
        this.items = items;
        this.context = context;

        filter(null);
    }

    @Override
    public int getCount() {
        return filteredItems.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        if (view == null) {
            view = inflateItemLayout();
        }
        String name = filteredItems.get(position).getName();
        ViewHolder holder = getViewHolder(view);


        holder.text.setText(name);
        if(name.length()>0)
            holder.letter.setText((name.charAt(0)+"").toUpperCase());
        if (flgSelection)
            if (selectedItems.contains(filteredItems.get(position)))
                holder.select.setChecked(true);
            else
                holder.select.setChecked(false);

        holder.select.setVisibility(flgSelection ? View.VISIBLE : View.GONE);


        return view;
    }

    public void filter(String text) {
        filterText = text;
        filteredItems.clear();
        for (Place item : items) {
            if (text == null || item != null && item.getName().toLowerCase().contains(text.toLowerCase())) {
                filteredItems.add(item);
            }

        }

        Collections.sort(filteredItems, new Comparator<Place>() {
            @Override
            public int compare(Place lhs, Place rhs) {
                return lhs.getName().compareTo(rhs.getName());
            }
        });

    }

    private View inflateItemLayout() {
        return LayoutInflater.from(context).inflate(LAYOUT_ITEM_RES, null);
    }

    private ViewHolder getViewHolder(View view) {
        ViewHolder holder;
        if (view.getTag() == null) {
            holder = new ViewHolder();
            holder.text = (TextView) view.findViewById(R.id.tvText);
            holder.letter = (TextView) view.findViewById(R.id.tvLetter);
            holder.select = (CheckBox) view.findViewById(R.id.cbSelect);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        return holder;
    }

    public List<Place> getSelectedItems() {
        return selectedItems;
    }

    public void clearSelection() {
        selectedItems.clear();
    }

    public void showSelection() {
        flgSelection = true;
    }

    public void hideSelection() {
        flgSelection = false;
    }

    public void switchCheckBox(Place item) {
        if (!selectedItems.contains(item))
            selectedItems.add(item);
        else
            selectedItems.remove(item);
    }

    public boolean isSelectionMode() {
        return flgSelection;
    }


    public List<Place> getFilteredItems() {
        return filteredItems;
    }

    @Override
    public void notifyDataSetChanged() {
        filter(filterText);
        super.notifyDataSetChanged();
    }


    private static class ViewHolder {
        TextView text;
        CheckBox select;
        TextView letter;
    }
}
