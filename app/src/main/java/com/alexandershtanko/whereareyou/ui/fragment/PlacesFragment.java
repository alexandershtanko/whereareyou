package com.alexandershtanko.whereareyou.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.alexandershtanko.whereareyou.R;
import com.alexandershtanko.whereareyou.model.Place;
import com.alexandershtanko.whereareyou.ui.Settings;
import com.alexandershtanko.whereareyou.ui.activity.MainActivity;
import com.alexandershtanko.whereareyou.ui.adapter.PlacesAdapter;
import com.alexandershtanko.whereareyou.ui.dialog.PlaceDialog;
import com.alexandershtanko.whereareyou.util.PrefsUtils;

import java.util.ArrayList;
import java.util.List;


public class PlacesFragment extends Fragment implements View.OnClickListener, BackPressed {


    private static final int REQUEST_CODE_PICK_CONTACT = 100;
    private static final String TAG = "ContactsFragment";
    private FloatingActionButton bAdd;
    private MainActivity activity;
    private ListView lvPlaces;
    private PlacesAdapter placesAdapter;

    private List<Place> places;
    private EditText etFilter;
    private View lFocus;

    public static Fragment newInstance() {
        return new PlacesFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_places, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_contacts, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (placesAdapter.isSelectionMode()) {
            menu.findItem(R.id.action_remove).setVisible(true);
            menu.findItem(R.id.action_edit).setVisible(false);
        } else {
            menu.findItem(R.id.action_remove).setVisible(false);
            menu.findItem(R.id.action_edit).setVisible(true);
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_remove:
                removeContactsFromContactList();
                break;
            case R.id.action_edit:
                showSelection();

        }
        return super.onOptionsItemSelected(item);
    }

    private List<Place> getPlaces() {
        List<Place> places;
        try {
            places = Settings.getPlaces(activity);

        } catch (PrefsUtils.PreferenceException e) {
            Log.e(TAG, "", e);
            places = new ArrayList<>();
        }
        return places;
    }

    private void setPlaces(List<Place> places) {
        try {
            Settings.setPlaces(activity, places);
        } catch (PrefsUtils.PreferenceException e) {
            Log.e(TAG, "", e);
        }
    }

    private void initView(View view) {
        places = getPlaces();

        bAdd = (FloatingActionButton) view.findViewById(R.id.bAdd);
        bAdd.setOnClickListener(this);
        lvPlaces = (ListView) view.findViewById(R.id.lvContacts);
        placesAdapter = new PlacesAdapter(activity, places);
        lvPlaces.setAdapter(placesAdapter);
        lvPlaces.setItemsCanFocus(true);
        lvPlaces.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Place place = placesAdapter.getFilteredItems().get(position);
                if (!placesAdapter.isSelectionMode()) {
                    openPlaceDialog(place);
                } else {
                    placesAdapter.switchCheckBox(place);
                    placesAdapter.notifyDataSetChanged();
                }
            }
        });

        lvPlaces.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                showSelection();
                return true;
            }
        });

        etFilter = (EditText) view.findViewById(R.id.tvFilter);
        etFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                placesAdapter.filter(etFilter.getText().toString());
                placesAdapter.notifyDataSetChanged();
            }
        });
        lFocus = view.findViewById(R.id.lFocus);
        etFilter.setText("");
        hideSoftKeyboard(activity, etFilter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bAdd:
                openPlaceDialog(null);
                break;
        }
    }

    private void openPlaceDialog(final Place place) {

        FragmentManager fm = getFragmentManager();
        PlaceDialog placeDialog = new PlaceDialog();
        placeDialog.setPlace(place);
        placeDialog.setOnDialogFinishedListener(new PlaceDialog.OnDialogFinishedListener() {
            @Override
            public void onSuccess(Place newPlace) {
                if (places.contains(place)) {
                    int ind = places.indexOf(place);
                    places.remove(ind);
                    places.add(ind, newPlace);
                } else
                    places.add(newPlace);

                setPlaces(places);
                placesAdapter.notifyDataSetChanged();
            }

            @Override
            public void onDismiss() {

            }
        });
        placeDialog.show(fm, null);
    }

    private void removeContactsFromContactList() {
        List<Place> selectedItems = placesAdapter.getSelectedItems();
        for (Place place : selectedItems) {
            places.remove(place);
        }
        setPlaces(places);
        hideSelection();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MainActivity) getActivity();
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.setActionBarTitle(R.string.my_places);
    }

    private boolean isSelectionMode() {
        return placesAdapter.isSelectionMode();
    }

    public boolean onBackPressed() {
        try {
            if (etFilter.getText().length() > 0 || etFilter.isFocused()) {
                clearFilter();
                return true;
            }

            if (isSelectionMode()) {
                hideSelection();
                return true;
            }
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
        return false;

    }

    private void clearFilter() {
        etFilter.setText("");
        hideSoftKeyboard(activity, etFilter);
    }

    private void hideSoftKeyboard(Activity activity, View view) {
        view.requestFocus();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
        view.clearFocus();
        lFocus.requestFocus();
    }

    private void hideSelection() {
        placesAdapter.clearSelection();
        placesAdapter.hideSelection();
        placesAdapter.notifyDataSetChanged();
        activity.invalidateOptionsMenu();
        bAdd.setVisibility(View.VISIBLE);
    }

    private void showSelection() {
        placesAdapter.showSelection();
        placesAdapter.notifyDataSetChanged();
        activity.invalidateOptionsMenu();
        bAdd.setVisibility(View.GONE);

    }
}
