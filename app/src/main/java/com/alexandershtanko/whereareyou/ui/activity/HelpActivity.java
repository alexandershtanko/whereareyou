package com.alexandershtanko.whereareyou.ui.activity;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alexandershtanko.whereareyou.R;
import com.alexandershtanko.whereareyou.ui.widget.RevealLayout;
import com.alexandershtanko.whereareyou.util.AppUtils;

/**
 * Created by alexander on 6/23/15.
 */
public class HelpActivity extends Activity {

    private final static int NUM_PAGES = 4;

    private final int[] textResArray = {
            R.string.help_text1,
            R.string.help_text2,
            R.string.help_text3,
            R.string.help_text4
    };
    private final int[] titleResArray = {
            R.string.app_name,
            R.string.title_text2,
            R.string.title_text3,
            R.string.title_text4
    };
    private final int[] colorResArray = {
            R.color.help_background1,
            R.color.help_background2,
            R.color.help_background3,
            R.color.help_background4,

    };
    private final int[] imageResArray = {
            R.drawable.icon,
            R.drawable.contacts,
            R.drawable.buildings,
            R.drawable.robots
    };

    private ViewPager helpPager;
    private PagerAdapter helpPagerAdapter;
    private RevealLayout rLayout;
    private View vBg;
    private View[] slideIndicators;
    private String appVersion = "";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        appVersion = AppUtils.getAppVerion(this);
        initView();
    }


    private void initView() {
        helpPager = (ViewPager) findViewById(R.id.helpPager);
        helpPagerAdapter = new HelpPagerAdapter();
        helpPager.setAdapter(helpPagerAdapter);
        rLayout = (RevealLayout) findViewById(R.id.rLayout);
        vBg = findViewById(R.id.vBg);
        vBg.setBackgroundColor(getResources().getColor(colorResArray[0]));
        rLayout.setBackgroundColor(getResources().getColor(colorResArray[0]));
        slideIndicators = new View[NUM_PAGES];
        slideIndicators[0] = findViewById(R.id.vSlideIndicator1);
        slideIndicators[1] = findViewById(R.id.vSlideIndicator2);
        slideIndicators[2] = findViewById(R.id.vSlideIndicator3);
        slideIndicators[3] = findViewById(R.id.vSlideIndicator4);

        slideIndicators[0].setBackgroundResource(R.drawable.ic_radio_button_checked_white_24dp);
        if (Build.VERSION.SDK_INT >= 21)
            getWindow().setStatusBarColor(getResources().getColor(colorResArray[0]));
        helpPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            int lastPosition = 0;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (Build.VERSION.SDK_INT >= 21)
                    getWindow().setStatusBarColor(getResources().getColor(colorResArray[(position) % 4]));
                vBg.setBackgroundColor(getResources().getColor(colorResArray[(position) % 4]));
                rLayout.setBackgroundColor(getResources().getColor(colorResArray[lastPosition % 4]));
                rLayout.hide(0);
                switch (position % 4) {
                    case 0:
                        rLayout.show(0, 0);
                        break;
                    case 1:
                        rLayout.show(rLayout.getWidth(), 0);
                        break;
                    case 2:
                        rLayout.show(0, rLayout.getHeight());
                        break;
                    case 3:
                        rLayout.show(rLayout.getWidth() / 2, rLayout.getHeight() / 2);
                        break;
                }

                for (int i = 0; i < slideIndicators.length; i++) {
                    if (i == position)
                        slideIndicators[i].setBackgroundResource(R.drawable.ic_radio_button_checked_white_24dp);
                    else
                        slideIndicators[i].setBackgroundResource(R.drawable.ic_radio_button_unchecked_white_24dp);
                }

                lastPosition = position;

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private class HelpPagerAdapter extends PagerAdapter implements View.OnClickListener {

        @Override
        public int getCount() {
            return NUM_PAGES;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            LinearLayout v = (LinearLayout) getLayoutInflater().inflate(R.layout.layout_help_slide, null);
            TextView tvText = (TextView) v.findViewById(R.id.tvText);
            TextView tvTtitle = (TextView) v.findViewById(R.id.tvTitle);
            ImageView ivImage = (ImageView) v.findViewById(R.id.ivImage);
            FloatingActionButton bFinish = (FloatingActionButton) v.findViewById(R.id.bNext);


            if (position + 1 < NUM_PAGES)
                bFinish.setVisibility(View.GONE);
            else {
                bFinish.setVisibility(View.VISIBLE);
                bFinish.setOnClickListener(this);
            }


            tvText.setText(textResArray[position]);
            tvTtitle.setText(titleResArray[position]);
            if (position == 0) {
                String titleWithVer = tvTtitle.getText().toString() + " " + appVersion;
                tvTtitle.setText(titleWithVer);
            }

            ivImage.setImageResource(imageResArray[position]);
            container.addView(v);
            return v;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }

        @Override
        public void onClick(View v) {
            finish();
        }
    }
}
