package com.alexandershtanko.whereareyou.ui.adapter;

import android.content.Context;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.alexandershtanko.whereareyou.R;
import com.alexandershtanko.whereareyou.model.Contact;
import com.alexandershtanko.whereareyou.util.ContactUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * Created by alexander on 6/16/15.
 */
public class ContactsAdapter extends BaseAdapter implements SwipeRefreshLayout.OnRefreshListener {
    private static final int LAYOUT_ITEM_RES = R.layout.listview_item_contacts;
    private final SwipeRefreshLayout srLayout;
    private List<String> contactIds = Collections.EMPTY_LIST;
    private HashMap<String, Contact> contactHashMap = new HashMap<>();
    private Context context;
    private List<String> selectedIds = new ArrayList<>();
    private boolean flgSelection = false;
    private List<String> filteredIds = new ArrayList<>();
    private String filterText;


    public ContactsAdapter(Context context, List<String> contactIds, SwipeRefreshLayout srLayout) {
        this.contactIds = contactIds;
        this.context = context;
        this.srLayout=srLayout;
        srLayout.setOnRefreshListener(this);
        srLayout.setEnabled(false);
        filter(null);
        updateContactCache();
    }

    @Override
    public int getCount() {
        return filteredIds.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredIds.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        if (view == null) {
            view = inflateItemLayout();
        }

        ViewHolder holder = getViewHolder(view);

        Contact contact = getContactById(filteredIds.get(position));
        if (contact.photoUri != null) {
            holder.photo.setImageURI(contact.photoUri);
            holder.letter.setVisibility(View.GONE);
            holder.photo.setVisibility(View.VISIBLE);
        }
        else {
            holder.photo.setVisibility(View.GONE);
            holder.letter.setVisibility(View.VISIBLE);
            if (contact.name != null && contact.name.length() > 0)
                holder.letter.setText((contact.name.charAt(0) + "").toUpperCase());
        }

        holder.name.setText(contact.name);
        if (flgSelection)
            if (selectedIds.contains(contact.id))
                holder.select.setChecked(true);
            else
                holder.select.setChecked(false);

        holder.select.setVisibility(flgSelection ? View.VISIBLE : View.GONE);


        return view;
    }

    public void filter(String text) {
        filterText = text;
        filteredIds.clear();
        for (String id : contactIds) {
            if (contactHashMap.containsKey(id)) {
                Contact contact = contactHashMap.get(id);
                if (text == null || contact.name != null && contact.name.toLowerCase().contains(text.toLowerCase())) {
                    filteredIds.add(id);
                }
            }

        }

        Collections.sort(filteredIds, new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {
                return contactHashMap.get(lhs).name.compareTo(contactHashMap.get(rhs).name);
            }
        });

    }

    private synchronized Contact getContactById(String id) {
        if (contactHashMap.containsKey(id))
            return contactHashMap.get(id);
        else {
            return new Contact();
        }
    }


    private View inflateItemLayout() {
        return LayoutInflater.from(context).inflate(LAYOUT_ITEM_RES, null);
    }

    private ViewHolder getViewHolder(View view) {
        ViewHolder holder;
        if (view.getTag() == null) {
            holder = new ViewHolder();
            holder.name = (TextView) view.findViewById(R.id.tvName);
            holder.photo = (ImageView) view.findViewById(R.id.ivPhoto);
            holder.select = (CheckBox) view.findViewById(R.id.cbSelect);
            holder.letter = (TextView) view.findViewById(R.id.tvLetter);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        return holder;
    }

    public List<String> getSelectedIds() {
        return selectedIds;
    }

    public void clearSelection() {
        selectedIds.clear();
    }

    public void showSelection() {
        flgSelection = true;
    }

    public void hideSelection() {
        flgSelection = false;
    }

    public void switchCheckBox(String contactId) {
        if (!selectedIds.contains(contactId))
            selectedIds.add(contactId);
        else
            selectedIds.remove(contactId);
    }

    public boolean isSelectionMode() {
        return flgSelection;
    }


    public void updateContactCache() {
        (new AsyncUpdateCache()).execute();
    }

    public List<String> getFilteredIds() {
        return filteredIds;
    }

    @Override
    public void notifyDataSetChanged() {
        filter(filterText);
        super.notifyDataSetChanged();
    }

    @Override
    public void onRefresh() {
        updateContactCache();
    }


    private static class ViewHolder {
        ImageView photo;
        TextView name;
        CheckBox select;
        TextView letter;
    }

    private class AsyncUpdateCache extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            if (contactIds != null)
                for (String id : contactIds) {
                    if (!contactHashMap.containsKey(id)) {
                        Contact contact = ContactUtils.getContactById(context.getContentResolver(), id);
                        try {
                            if (MediaStore.Images.Media.getBitmap(context.getContentResolver(), contact.photoUri) == null)
                                contact.photoUri = null;
                        } catch (Exception e) {
                            contact.photoUri = null;
                        }

                        contactHashMap.put(id, contact);
                    }
                }
            return null;
        }

        @Override
        protected void onPreExecute() {
            TypedValue typed_value = new TypedValue();
            context.getTheme().resolveAttribute(android.support.v7.appcompat.R.attr.actionBarSize, typed_value, true);
            srLayout.setProgressViewOffset(false, 0, context.getResources().getDimensionPixelSize(typed_value.resourceId));
            srLayout.setRefreshing(true);
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            srLayout.setRefreshing(false);
            notifyDataSetChanged();
            super.onPostExecute(aVoid);
        }
    }

}
