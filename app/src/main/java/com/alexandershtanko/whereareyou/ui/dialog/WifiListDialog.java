package com.alexandershtanko.whereareyou.ui.dialog;


import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.alexandershtanko.whereareyou.R;
import com.alexandershtanko.whereareyou.model.Wifi;
import com.alexandershtanko.whereareyou.ui.adapter.CheckListAdapter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by alexander on 6/17/15.
 */
public class WifiListDialog extends DialogFragment implements View.OnClickListener {

    private static final String KEY_WIFI_LIST = "key wifi list";
    private List<String> wifiStringList;
    private List<Wifi> wifiList;

    private OnDialogFinishedListener onDialogFinishedListener;
    private Activity activity;
    private Button bDelete;
    private Button bDismiss;
    private ListView lvWifiList;
    private CheckListAdapter wifiListAdapter;

    public static WifiListDialog newInstance(List<Wifi> wifiList) {

        Bundle args = new Bundle();
        ArrayList<String> list = new ArrayList<>();
        for (Wifi wifi : wifiList) {
            list.add(wifi.BSSID + " " + wifi.SSID);
        }
        args.putStringArrayList(KEY_WIFI_LIST, list);
        WifiListDialog fragment = new WifiListDialog();
        fragment.wifiList = wifiList;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_wifi_list, container);
        if (getArguments() != null && getArguments().containsKey(KEY_WIFI_LIST))
            wifiStringList = getArguments().getStringArrayList(KEY_WIFI_LIST);
        else
            wifiStringList = new ArrayList<>();

        initView(view);
        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
    }

    private void initView(View view) {
        lvWifiList = (ListView) view.findViewById(R.id.lvWifiList);
        wifiListAdapter = new CheckListAdapter(activity, wifiStringList);
        lvWifiList.setAdapter(wifiListAdapter);
        lvWifiList.setItemsCanFocus(true);
        lvWifiList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                wifiListAdapter.switchCheckBox(wifiStringList.get(position));
                wifiListAdapter.notifyDataSetChanged();
            }
        });


        bDelete = (Button) view.findViewById(R.id.bDelete);
        bDismiss = (Button) view.findViewById(R.id.bDismiss);
        bDelete.setOnClickListener(this);
        bDismiss.setOnClickListener(this);
    }


    public void setOnDialogFinishedListener(OnDialogFinishedListener onDialogFinishedListener) {
        this.onDialogFinishedListener = onDialogFinishedListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bDelete:
                List<String> itemsToDelete = wifiListAdapter.getSelectedItems();
                Iterator<Wifi> i = wifiList.iterator();
                while (i.hasNext()) {
                    Wifi s = i.next();
                    if (itemsToDelete.contains(s.BSSID + " " + s.SSID))
                        i.remove();
                }

                if (onDialogFinishedListener != null) onDialogFinishedListener.onSuccess(wifiList);
                dismiss();
                break;
            case R.id.bDismiss:
                if (onDialogFinishedListener != null) onDialogFinishedListener.onDismiss();
                dismiss();
                break;
        }
    }

    public interface OnDialogFinishedListener {
        void onSuccess(List<Wifi> wifiList);

        void onDismiss();
    }
}
