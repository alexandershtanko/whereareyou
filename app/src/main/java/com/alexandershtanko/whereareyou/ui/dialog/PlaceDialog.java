package com.alexandershtanko.whereareyou.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.alexandershtanko.whereareyou.R;
import com.alexandershtanko.whereareyou.location.LocationTracker;
import com.alexandershtanko.whereareyou.model.LocationPack;
import com.alexandershtanko.whereareyou.model.Place;
import com.alexandershtanko.whereareyou.model.Wifi;
import com.alexandershtanko.whereareyou.util.DrawableUtils;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;
import org.osmdroid.api.IMapController;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.OverlayItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexander on 6/17/15.
 */
public class PlaceDialog extends DialogFragment implements View.OnClickListener {

    private static final String TAG = "PlaceDialog";
    private Place place = new Place();

    private OnDialogFinishedListener onDialogFinishedListener;
    private EditText etName;
    private TextView tvGps;
    private TextView tvBaseStation;
    private TextView tvWifi;
    private MapView mvGps;
    private Activity activity;
    private Button bSave;

    private Button bEditWifiList;
    private Button bClearGps;
    private Button bClearBaseStation;
    private Button bUpdate;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_place, container);
        getDialog().setTitle(R.string.place);
        initView(view);
        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
    }

    private void initView(View view) {
        etName = (EditText) view.findViewById(R.id.etName);
        tvGps = (TextView) view.findViewById(R.id.tvGps);
        tvBaseStation = (TextView) view.findViewById(R.id.tvBaseStation);
        tvWifi = (TextView) view.findViewById(R.id.tvWifi);
        mvGps = (MapView) view.findViewById(R.id.mvGps);
        mvGps.setTileSource(TileSourceFactory.MAPNIK);

        bSave = (Button) view.findViewById(R.id.bSave);
        bSave.setOnClickListener(this);
        bUpdate = (Button) view.findViewById(R.id.bUpdate);
        bUpdate.setOnClickListener(this);

        bEditWifiList = (Button) view.findViewById(R.id.bEditWifiList);
        bEditWifiList.setOnClickListener(this);
        bClearGps=(Button)view.findViewById(R.id.bClearGps);
        bClearBaseStation=(Button)view.findViewById(R.id.bClearBaseStation);
        bClearGps.setOnClickListener(this);
        bClearBaseStation.setOnClickListener(this);
        populateViews();


    }

    private void populateViews() {

        etName.setText(place.getName());
        if (place.getLocationPack() != null) {
            updateView();
        } else {
            new GetLocationTask().execute();
        }

    }

    private void updateView() {

        LocationPack locationPack = place.getLocationPack();
        String sGps = "";
        if (locationPack.getGpsLocation() != null) {
            sGps += activity.getString(R.string.latitude) + ":" + locationPack.getGpsLocation().getLatitude() + " ";
            sGps += activity.getString(R.string.longitude) + ":" + locationPack.getGpsLocation().getLongitude() + "\n";
            updateMapView(locationPack);
            mvGps.setVisibility(View.VISIBLE);
        }
        else mvGps.setVisibility(View.GONE);

        tvGps.setText(sGps);

        String sBS = "";
        if (locationPack.getCell() != null) {

            if (locationPack.getCell().getPhoneType() == TelephonyManager.PHONE_TYPE_GSM) {
                sBS += " \nCID:"+(locationPack.getCell()).getCid()+" LAC:"+(locationPack.getCell()).getLac();


            }
            if (locationPack.getCell().getPhoneType() == TelephonyManager.PHONE_TYPE_CDMA) {
                sBS += " " + (locationPack.getCell()).getBaseStationId();

            }

        }
        tvBaseStation.setText(activity.getText(R.string.base_station)+sBS);

        String sWifi = " (0)";
        if (locationPack.getWifiList() != null) {
           sWifi=" ("+locationPack.getWifiList().size()+")";
        }
        tvWifi.setText(activity.getText(R.string.wifi_points)+sWifi);
    }

    private void updateMapView(LocationPack locationPack) {
        IMapController mapController = mvGps.getController();
        mapController.setZoom(18);
        GeoPoint startPoint = new GeoPoint(locationPack.getGpsLocation().getLatitude(), locationPack.getGpsLocation().getLongitude(), locationPack.getGpsLocation().getAltitude());
        mapController.setCenter(startPoint);

        OverlayItem myLocationOverlayItem = new OverlayItem("Here", "Current Position", startPoint);
        Drawable myCurrentLocationMarker = DrawableUtils.getScaledDrawable(activity, R.drawable.place,70,100);
        myLocationOverlayItem.setMarker(myCurrentLocationMarker);

        final ArrayList<OverlayItem> items = new ArrayList<>();
        items.add(myLocationOverlayItem);


        ResourceProxy resourceProxy = new DefaultResourceProxyImpl(activity);
        ItemizedIconOverlay<OverlayItem> currentLocationOverlay = new ItemizedIconOverlay<>(items,
                new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>() {
                    public boolean onItemSingleTapUp(final int index, final OverlayItem item) {
                        return true;
                    }

                    public boolean onItemLongPress(final int index, final OverlayItem item) {
                        return true;
                    }
                }, resourceProxy);
        mvGps.getOverlays().clear();
        mvGps.getOverlays().add(currentLocationOverlay);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void setPlace(Place place) {
        this.place = new Place();
        if (place != null) {
            this.place.setName(place.getName());
            this.place.setLocationPack(place.getLocationPack());
        }


    }

    private void updateLocation() {
        this.place.setLocationPack(LocationTracker.getLocationPack(activity,LocationTracker.MIN_DISCOVERY_TIME,PlaceDialog.TAG));
    }

    public void setOnDialogFinishedListener(OnDialogFinishedListener onDialogFinishedListener) {
        this.onDialogFinishedListener = onDialogFinishedListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bSave:
                if (etName.getText().length() == 0) {
                    etName.setError(activity.getString(R.string.set_place_name));
                    break;
                }
                place.setName(etName.getText().toString());
                if (onDialogFinishedListener != null) onDialogFinishedListener.onSuccess(place);
                dismiss();
                break;
            case R.id.bUpdate: new GetLocationTask().execute(); break;
            case R.id.bEditWifiList:
                showWifiListDialog();
                break;
            case R.id.bClearGps: clearGps(); break;
            case R.id.bClearBaseStation:clearBaseStation();break;
        }
    }

    private void clearBaseStation() {
        place.getLocationPack().setCell(null);
        updateView();
    }

    private void clearGps() {
        place.getLocationPack().setGpsLocation(null);
        updateView();
    }

    private void showWifiListDialog() {
        FragmentManager fm = getFragmentManager();
        WifiListDialog wifiListDialog = WifiListDialog.newInstance(place.getLocationPack().getWifiList());

        wifiListDialog.setOnDialogFinishedListener(new WifiListDialog.OnDialogFinishedListener() {
            @Override
            public void onSuccess(List<Wifi> wifiList) {
                place.getLocationPack().setWifiList(wifiList);
                updateView();
            }

            @Override
            public void onDismiss() {

            }
        });
        wifiListDialog.show(fm, null);
    }





    public interface OnDialogFinishedListener {
        void onSuccess(Place newPlace);

        void onDismiss();
    }

    private class GetLocationTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            updateLocation();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            bSave.setEnabled(false);
            bUpdate.setEnabled(false);
            bEditWifiList.setEnabled(false);
            bClearBaseStation.setEnabled(false);
            bClearGps.setEnabled(false);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (isDetached())
                return;
            try {


                bSave.setEnabled(true);
                bUpdate.setEnabled(true);
                bEditWifiList.setEnabled(true);
                bClearBaseStation.setEnabled(true);
                bClearGps.setEnabled(true);

                updateView();
            } catch (Exception e) {
                Log.e(TAG, "", e);
            }
        }
    }
}
