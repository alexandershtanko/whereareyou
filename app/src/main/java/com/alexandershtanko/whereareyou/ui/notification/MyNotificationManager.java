package com.alexandershtanko.whereareyou.ui.notification;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.alexandershtanko.whereareyou.R;
import com.alexandershtanko.whereareyou.ui.activity.MainActivity;

/**
 * Created by alexander on 7/2/15.
 */
public class MyNotificationManager {
    public static final int LOCATION_NOTIFY_ID = 100;
    private static final String TAG = "MyNotificationManager";

    public static Notification showLocationNotification(Context context,String text) {
        Intent notificationIntent = new Intent(context, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context,
                0, notificationIntent,
                PendingIntent.FLAG_NO_CREATE);

        Resources res = context.getResources();
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.drawable.ic_explore_white_24dp)
                        // большая картинка
                .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.icon))
                        //.setTicker(res.getString(R.string.warning)) // текст в строке состояния
                .setTicker(context.getString(R.string.search_location))
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                        //.setContentTitle(res.getString(R.string.notifytitle)) // Заголовок уведомления
                .setContentTitle(context.getString(R.string.app_name))
                        //.setContentText(res.getString(R.string.notifytext))
                .setContentText(context.getString(R.string.search_location) + " " + text);

        Notification notification = builder.build();
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
            notification.contentIntent=contentIntent;
        }

            NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(LOCATION_NOTIFY_ID, notification);
        return notification;
    }

    public static void hideLocationNotification(Context context) {
        try {
            NotificationManager notificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(LOCATION_NOTIFY_ID);
        }
        catch (Exception e)
        {
            Log.e(TAG,"",e);
        }
    }


}
