package com.alexandershtanko.whereareyou.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.alexandershtanko.whereareyou.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexander on 6/16/15.
 */
public class CheckListAdapter extends BaseAdapter {
    private static final int LAYOUT_ITEM_RES = R.layout.listview_item_checklist;

    private List<String> items;
    private Context context;
    private List<String> selectedItems = new ArrayList<>();
    private boolean flgSelection = true;




    public CheckListAdapter(Context context, List<String> items) {
        this.items = items;
        this.context = context;


    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        if (view == null) {
            view = inflateItemLayout();
        }
        String name = items.get(position);
        ViewHolder holder = getViewHolder(view);


        holder.text.setText(name);
        if (flgSelection)
            if (selectedItems.contains(items.get(position)))
                holder.select.setChecked(true);
            else
                holder.select.setChecked(false);

        holder.select.setVisibility(flgSelection ? View.VISIBLE : View.GONE);


        return view;
    }



    private View inflateItemLayout() {
        return LayoutInflater.from(context).inflate(LAYOUT_ITEM_RES, null);
    }

    private ViewHolder getViewHolder(View view) {
        ViewHolder holder;
        if (view.getTag() == null) {
            holder = new ViewHolder();
            holder.text = (TextView) view.findViewById(R.id.tvText);
            holder.select = (CheckBox) view.findViewById(R.id.cbSelect);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        return holder;
    }

    public List<String> getSelectedItems() {
        return selectedItems;
    }


    public void showSelection() {
        flgSelection = true;
    }

    public void hideSelection() {
        flgSelection = false;
    }

    public void switchCheckBox(String item) {
        if (!selectedItems.contains(item))
            selectedItems.add(item);
        else
            selectedItems.remove(item);
    }

    public boolean isSelectionMode() {
        return flgSelection;
    }

    private static class ViewHolder {
        TextView text;
        CheckBox select;

    }
}
