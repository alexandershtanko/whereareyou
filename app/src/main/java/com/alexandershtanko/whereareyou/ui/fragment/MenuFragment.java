package com.alexandershtanko.whereareyou.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.alexandershtanko.whereareyou.R;
import com.alexandershtanko.whereareyou.ui.activity.MainActivity;

public class MenuFragment extends Fragment implements BackPressed, View.OnClickListener {

    private static final String TAG = "MenuFragment";
    private MainActivity activity;

    public static Fragment newInstance() {
        return new MenuFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MainActivity) getActivity();
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.setActionBarTitle(R.string.app_name);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        Button bContacts = (Button) view.findViewById(R.id.bContacts);
        Button bPlaces = (Button) view.findViewById(R.id.bPlaces);
        Button bSettings = (Button) view.findViewById(R.id.bSettings);
        Button bHistory = (Button) view.findViewById(R.id.bHistory);
        Button bHelp = (Button) view.findViewById(R.id.bHelp);


        bContacts.setOnClickListener(this);
        bPlaces.setOnClickListener(this);
        bSettings.setOnClickListener(this);
        bHistory.setOnClickListener(this);
        bHelp.setOnClickListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bContacts:
                activity.selectContactsFragment();
                break;
            case R.id.bPlaces:
                activity.selectPlacesFragment();
                break;
            case R.id.bSettings:
                activity.openSettings();
                break;
            case R.id.bHistory:
                activity.selectHistoryFragment();
                break;
            case R.id.bHelp:
                activity.showHelp();
                break;
        }
    }
}
