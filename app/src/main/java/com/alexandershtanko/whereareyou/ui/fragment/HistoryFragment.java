package com.alexandershtanko.whereareyou.ui.fragment;


import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.alexandershtanko.whereareyou.R;
import com.alexandershtanko.whereareyou.model.SmsMessage;
import com.alexandershtanko.whereareyou.ui.activity.MainActivity;
import com.alexandershtanko.whereareyou.ui.adapter.HistoryAdapter;

import java.util.ArrayList;
import java.util.List;

public class HistoryFragment extends Fragment implements BackPressed {

    private static final String TAG = "HistoryFragment";

    private MainActivity activity;
    private ListView lvHistory;
    private HistoryAdapter historyAdapter;

    private EditText etFilter;
    private SwipeRefreshLayout srLayout;
    private View lFocus;
    private List<SmsMessage> smsMessageList = new ArrayList<>();

    public static Fragment newInstance() {
        return new HistoryFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_history, container, false);
        initView(view);
        populateSmsMessageList();
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_contacts, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (historyAdapter.isSelectionMode()) {
            menu.findItem(R.id.action_remove).setVisible(true);
            menu.findItem(R.id.action_edit).setVisible(false);
        } else {
            menu.findItem(R.id.action_remove).setVisible(false);
            menu.findItem(R.id.action_edit).setVisible(true);
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_remove:
                removeSmsMessagesFromSmsMessageList();

                break;
            case R.id.action_edit:
                showSelection();

        }
        return super.onOptionsItemSelected(item);
    }

    private void removeSmsMessagesFromSmsMessageList() {
        List<SmsMessage> list = historyAdapter.getSelectedItems();
        for (SmsMessage smsMessage : list) {
            smsMessage.delete();
        }
        populateSmsMessageList();
        hideSelection();
    }

    private void initView(View view) {

        srLayout = (SwipeRefreshLayout) view.findViewById(R.id.srLayout);


        lvHistory = (ListView) view.findViewById(R.id.lvHistory);
        historyAdapter = new HistoryAdapter(activity, smsMessageList, srLayout);
        lvHistory.setAdapter(historyAdapter);
        lvHistory.setItemsCanFocus(true);
        lvHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SmsMessage smsMessage = (SmsMessage) historyAdapter.getItem(position);
                if (historyAdapter.isSelectionMode()) {
                    historyAdapter.switchCheckBox(smsMessage);
                    historyAdapter.notifyDataSetChanged();
                }
            }
        });

        lvHistory.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                showSelection();
                return true;
            }
        });

        etFilter = (EditText) view.findViewById(R.id.tvFilter);
        etFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                historyAdapter.filter(etFilter.getText().toString());
            }
        });
        lFocus = view.findViewById(R.id.lFocus);
        hideSoftKeyboard(activity, etFilter);

    }

    private void hideSoftKeyboard(Activity activity, View view) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
        view.clearFocus();
        lFocus.requestFocus();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MainActivity) getActivity();
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.setActionBarTitle(R.string.history);
    }

    private boolean isSelectionMode() {
        return historyAdapter.isSelectionMode();
    }

    public boolean onBackPressed() {
        try {
            if (etFilter.getText().length() > 0 || etFilter.isFocused()) {
                clearFilter();
                return true;
            }

            if (isSelectionMode()) {
                hideSelection();
                return true;
            }
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
        return false;

    }

    private void clearFilter() {
        etFilter.setText("");
        hideSoftKeyboard(activity, etFilter);
    }

    private void hideSelection() {
        historyAdapter.clearSelection();
        historyAdapter.hideSelection();
        historyAdapter.notifyDataSetChanged();
        activity.invalidateOptionsMenu();

    }

    private void showSelection() {
        historyAdapter.showSelection();
        historyAdapter.notifyDataSetChanged();
        activity.invalidateOptionsMenu();


    }

    public void populateSmsMessageList() {
        (new AsyncSmsMessageTask()).execute();
    }

    private class AsyncSmsMessageTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                smsMessageList.clear();
                List<SmsMessage> list = SmsMessage.find(SmsMessage.class, null, null, null, "date_add asc", null);
                for (SmsMessage smsMessage : list) {
                    smsMessageList.add(smsMessage);
                }
                return true;
            } catch (Exception e) {
                Log.e(TAG, "", e);
            }
            return false;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Boolean res) {
            historyAdapter.filter(null);
            historyAdapter.onRefresh();
            super.onPostExecute(res);
        }
    }
}
