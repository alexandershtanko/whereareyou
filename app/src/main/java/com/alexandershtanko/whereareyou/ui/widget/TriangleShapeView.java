package com.alexandershtanko.whereareyou.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

import com.alexandershtanko.whereareyou.R;

public class TriangleShapeView extends View {

    private int corner = 0;
    private int color = 0;
    Path path = new Path();
    Paint p = new Paint();
    public TriangleShapeView(Context context) {
        super(context);
    }

    public TriangleShapeView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public TriangleShapeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.TriangleShapeView,
                0, 0);

        try {
            color = a.getColor(R.styleable.TriangleShapeView_bgColor, context.getResources().getColor(R.color.help_background1));
            corner = a.getInteger(R.styleable.TriangleShapeView_corner, 0);
        } finally {
            a.recycle();
        }
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int w = getWidth();
        int h = getHeight();

        switch (corner) {
            case 1:
                path.moveTo(0, 0);
                path.lineTo(w, 0);
                path.lineTo(w, h);
                path.lineTo(0, 0);
                break;
            case 2:
                path.moveTo(0, 0);
                path.lineTo(0, h);
                path.lineTo(w, h);
                path.lineTo(0, 0);
                break;
            case 3:
                path.moveTo(0, h);
                path.lineTo(w, h);
                path.lineTo(w, 0);
                path.lineTo(0, h);
                break;

            case 0:
            default:
                path.moveTo(0, 0);
                path.lineTo(0, h);
                path.lineTo(w, 0);
                path.lineTo(0, 0);
                break;
        }
        path.close();


        if (color != 0)
            p.setColor(color);

        canvas.drawPath(path, p);
    }
}