package com.alexandershtanko.whereareyou.ui.fragment;

/**
 * Created by
 *
 * @author Alexander Shtanko
 *         on 10/26/15.
 */
public interface BackPressed {
    boolean onBackPressed();
}
