package com.alexandershtanko.whereareyou.ui.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.alexandershtanko.whereareyou.R;
import com.alexandershtanko.whereareyou.billing.PurchaseHelper;
import com.alexandershtanko.whereareyou.model.PurchaseConstants;
import com.alexandershtanko.whereareyou.ui.Settings;
import com.alexandershtanko.whereareyou.ui.fragment.BackPressed;
import com.alexandershtanko.whereareyou.ui.fragment.ContactsFragment;
import com.alexandershtanko.whereareyou.ui.fragment.HistoryFragment;
import com.alexandershtanko.whereareyou.ui.fragment.MenuFragment;
import com.alexandershtanko.whereareyou.ui.fragment.PlacesFragment;
import com.alexandershtanko.whereareyou.ui.widget.RevealLayout;
import com.alexandershtanko.whereareyou.util.GpsUtils;
import com.alexandershtanko.whereareyou.util.billing.Purchase;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private PurchaseHelper purchaseHelper;
    private Fragment currentFragment;
    private RevealLayout rLayout;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        hideShadow();
        initView();
        if (Settings.firstStart(this)) {
            showHelp();
            restorePurchase();
        }

        GpsUtils.checkForGps(this);


    }

    private void restorePurchase() {
        purchaseHelper = new PurchaseHelper(this);
        final PurchaseHelper.OnConsumeResultListener onConsumeResultListener = new PurchaseHelper.OnConsumeResultListener() {
            @Override
            public void onSuccess(Purchase info) {
                Settings.setPurchased(MainActivity.this, true);
                Toast.makeText(MainActivity.this, R.string.purchase_successed, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(String msg) {
                Log.e(TAG, msg);
            }
        };

        PurchaseHelper.OnInitListener onInitListener = new PurchaseHelper.OnInitListener() {
            @Override
            public void onSuccess() {
                purchaseHelper.consumeItem(PurchaseConstants.SKU, onConsumeResultListener);
            }

            @Override
            public void onError(String msg) {
                Log.e(TAG, msg);
            }
        };

        purchaseHelper.init(onInitListener);

    }


    private void initView() {
        rLayout = (RevealLayout) findViewById(R.id.flContainer);
        mAdView = (AdView) findViewById(R.id.adView);
        mAdView.setVisibility(View.GONE);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                mAdView.setVisibility(View.GONE);
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }
        });
        selectMenuFragment();
    }

    public void showHelp() {

        startActivity(new Intent(this, HelpActivity.class));

    }

    private void hideShadow() {
        try {
            getSupportActionBar().setElevation(0);
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (purchaseHelper != null)
            purchaseHelper.release();
    }

    @Override
    public void onBackPressed() {

        boolean flgBack = true;
        try {
            BackPressed fragment = (BackPressed) currentFragment;
            if (fragment != null) {
                flgBack = !fragment.onBackPressed();
            }
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }

        if (flgBack)
            super.onBackPressed();
    }


    //======================================================================================

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        invalidateOptionsMenu();

        if (!Settings.getPurchased(this)) {
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    protected boolean onPrepareOptionsPanel(View view, Menu menu) {
        boolean isEnabled = Settings.getEnabled(this);
        menu.findItem(R.id.action_enable).setIcon(isEnabled ? R.mipmap.ic_launcher : R.mipmap.ic_launcher_black);
        menu.findItem(R.id.action_enable).setTitle(isEnabled ? R.string.active : R.string.non_active);
        return super.onPrepareOptionsPanel(view, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_enable:
                boolean isEnabled = !Settings.getEnabled(this);
                Settings.setEnabled(this, isEnabled);
                Toast.makeText(this, isEnabled ? getString(R.string.service_started) : getString(R.string.service_stoped), Toast.LENGTH_LONG).show();
                invalidateOptionsMenu();
                break;
            case R.id.action_settings:
                openSettings();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void openSettings() {
        startActivity(new Intent(this, SettingsActivity.class));
    }

    public void selectMenuFragment() {
        selectFragment(MenuFragment.newInstance(), false);
    }

    public void selectContactsFragment() {
        selectFragment(ContactsFragment.newInstance(), true);
    }

    public void selectPlacesFragment() {
        selectFragment(PlacesFragment.newInstance(), true);
    }

    public void selectHistoryFragment() {
        selectFragment(HistoryFragment.newInstance(), true);
    }

    private void selectFragment(Fragment fragment, boolean addToBackStack) {
        currentFragment = fragment;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.flContainer, fragment);
        if (addToBackStack)
            transaction.addToBackStack(null);
        transaction.commit();


        rLayout.post(new Runnable() {
            @Override
            public void run() {
                rLayout.showFromPointer();
            }
        });

    }


    public void setActionBarTitle(int titleRes) {
        try {
            getSupportActionBar().setTitle(titleRes);
        } catch (NullPointerException e) {
            Log.e(TAG, "", e);
        }
    }
}
