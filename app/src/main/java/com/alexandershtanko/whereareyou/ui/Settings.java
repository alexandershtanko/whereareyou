package com.alexandershtanko.whereareyou.ui;

import android.content.Context;
import android.util.Log;

import com.alexandershtanko.whereareyou.model.Place;
import com.alexandershtanko.whereareyou.util.AppUtils;
import com.alexandershtanko.whereareyou.util.PrefsUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by aleksandr on 04.06.15.
 */
public class Settings {
    private static final String TAG = "Settings";
    private static final String PREF_PLACES = "Places";
    private static final String PREF_CONTACTS = "Contacts";
    private static final String PREF_ENABLED = "Enabled";
    private static final String PREF_PURCHASED = "Purchased";
    private static final String PREF_VERSION = "Version";
    private static final String PREF_INTERCEPT_ENABLED = "Intercept";
    private static final String PREF_LOCATION_PERIOD = "LocationPeriod";


    private static final int DEFAULT_LOCATION_PERIOD = 60000;


    public static void setPlaces(Context context, List<Place> places) throws PrefsUtils.PreferenceException {
        PrefsUtils.putObject(context, PREF_PLACES, places);
    }

    public static List<Place> getPlaces(Context context) throws PrefsUtils.PreferenceException {
        try {

            return (ArrayList<Place>) PrefsUtils.getObjectList(context, PREF_PLACES, Place.class);
        } catch (Exception e) {
            throw new PrefsUtils.PreferenceException(e);
        }
    }

    public static void setContactIds(Context context, Set<String> contactIdList) throws PrefsUtils.PreferenceException {
        PrefsUtils.putStringSet(context, PREF_CONTACTS, contactIdList);
    }

    public static Set<String> getContactIds(Context context) throws PrefsUtils.PreferenceException {
        try {
            return PrefsUtils.getStringSet(context, PREF_CONTACTS);
        } catch (Exception e) {
            throw new PrefsUtils.PreferenceException(e);
        }
    }

    public static void setEnabled(Context context, boolean enabled) {
        PrefsUtils.putBoolean(context, PREF_ENABLED, enabled);
    }

    public static boolean getEnabled(Context context) {
        return PrefsUtils.getBoolean(context, PREF_ENABLED, true);
    }

    public static long getLocationPeriod(Context context) {
        int res = DEFAULT_LOCATION_PERIOD;
        try {
            res = Integer.parseInt(PrefsUtils.getString(context, PREF_LOCATION_PERIOD, String.valueOf(DEFAULT_LOCATION_PERIOD)));
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
        return res;
    }

    public static boolean firstStart(Context context) {
        String version = PrefsUtils.getString(context, PREF_VERSION);
        String currVersion = AppUtils.getAppVerion(context);
        if (!version.equals(currVersion)) {
            PrefsUtils.putString(context, PREF_VERSION, currVersion);
            return true;
        } else {
            return false;
        }

    }

    public static boolean isInterceptEnabled(Context context) {
        return PrefsUtils.getBoolean(context, PREF_INTERCEPT_ENABLED, false);
    }


    public static void setPurchased(Context context, boolean purchased) {
        PrefsUtils.putBoolean(context, PREF_PURCHASED, purchased);
    }

    public static boolean getPurchased(Context context) {
        return PrefsUtils.getBoolean(context, PREF_PURCHASED, false);
    }
}
