package com.alexandershtanko.whereareyou.model;

import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexander on 6/16/15.
 */
public class Contact {
    //Не изменять!
    public String name = "";
    public List<String> phoneList = new ArrayList<>();
    public List<String> emailList = new ArrayList<>();
    public String id = "";
    public Uri photoUri=null;
}
