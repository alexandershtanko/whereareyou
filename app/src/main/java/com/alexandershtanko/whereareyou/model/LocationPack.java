package com.alexandershtanko.whereareyou.model;


import android.location.Location;
import android.net.wifi.ScanResult;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by alexander on 6/9/15.
 */
public class LocationPack implements Serializable {
    //Не изменять!
    private static final float SAME_GPS_DISTANCE = 10;
    private static final float NEAR_GPS_DISTANCE = 100;
    private static final String TAG = "LocationPack";
    private GpsLocation gpsLocation = null;
    private List<Wifi> wifiList = Collections.EMPTY_LIST;
    private Cell cell = null;
    private String locationName = "";


    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public List<Wifi> getWifiList() {
        return wifiList;
    }

    public void setWifiList(List<Wifi> wifiList) {
        this.wifiList = wifiList;
    }

    @JsonIgnore
    public void setWifiListFromScanResultList(List<ScanResult> wifiList) {
        this.wifiList = new ArrayList<>();
        if (wifiList != null)
            for (ScanResult w : wifiList) {
                Wifi wifi = new Wifi();
                wifi.BSSID = w.BSSID;
                wifi.SSID = w.SSID;
                this.wifiList.add(wifi);
            }
    }

    public Cell getCell() {
        return cell;
    }

    public void setCell(Cell cell) {
        this.cell = cell;
    }

    public GpsLocation getGpsLocation() {
        return gpsLocation;
    }

    public void setGpsLocation(GpsLocation gpsLocation) {
        this.gpsLocation = gpsLocation;
    }

    @JsonIgnore
    public void setLocation(Location location) {
        if (location != null) {
            gpsLocation = new GpsLocation();
            gpsLocation.setAltitude(location.getAltitude());
            gpsLocation.setLongitude(location.getLongitude());
            gpsLocation.setLatitude(location.getLatitude());
            gpsLocation.setAccuracy(location.getAccuracy());
        } else
            setGpsLocation(null);
    }

    @JsonIgnore
    public void setCell(CellLocation cellLocation, int phoneType) {
        try {
            if (cellLocation != null) {
                cell = new Cell();
                cell.setPhoneType(phoneType);
                if (phoneType == TelephonyManager.PHONE_TYPE_GSM) {

                    cell.setCid(((GsmCellLocation) cellLocation).getCid());
                    cell.setLac(((GsmCellLocation) cellLocation).getLac());
                } else if (phoneType == TelephonyManager.PHONE_TYPE_CDMA) {
                    cell.setBaseStationId(((CdmaCellLocation) cellLocation).getBaseStationId());
                    cell.setNetworkId(((CdmaCellLocation) cellLocation).getNetworkId());
                    cell.setSystemId(((CdmaCellLocation) cellLocation).getSystemId());
                }
            } else
                setCell(null);

        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
    }

    public ComparisonResult compare(LocationPack locationPack) {
        ComparisonResult res = ComparisonResult.Far;

        List<Wifi> wifiList = locationPack.getWifiList();
        GpsLocation gpsLocation = locationPack.getGpsLocation();
        Cell cell = locationPack.getCell();


        if (wifiList != null && getWifiList() != null) {
            for (Wifi searchWifi : getWifiList()) {
                for (Wifi wifi : wifiList) {
                    if (searchWifi.BSSID.equals(wifi.BSSID)) {
                        res = ComparisonResult.Same;
                        break;
                    }
                }
                if (res == ComparisonResult.Same)
                    break;
            }
        }

        if (cell != null && getCell() != null && res == ComparisonResult.Far) {
            int phoneType = locationPack.getCell().getPhoneType();
            if (phoneType == TelephonyManager.PHONE_TYPE_GSM) {
                if (cell.getCid() == (getCell()).getCid() &&
                        (cell.getLac() == (getCell()).getLac())) {

                    res = ComparisonResult.Near;
                }
            } else if (phoneType == TelephonyManager.PHONE_TYPE_CDMA) {
                if ((cell).getBaseStationId() == (getCell()).getBaseStationId() &&
                        (cell).getNetworkId() == (getCell()).getNetworkId() &&
                        (cell).getSystemId() == (getCell()).getSystemId()) {

                    res = ComparisonResult.Near;
                }
            }
        }

        if (gpsLocation != null && getGpsLocation() != null && res != ComparisonResult.Near) {
            float distance = gpsLocation.distanceTo(getGpsLocation());
            if (distance < SAME_GPS_DISTANCE)
                res = ComparisonResult.Same;
            else if (distance < NEAR_GPS_DISTANCE && res == ComparisonResult.Far)
                res = ComparisonResult.Near;
        }

        return res;
    }

    @JsonIgnore
    public float getApproximateAccuracy() {
        if (gpsLocation != null)
            return gpsLocation.getAccuracy();
        else if (cell != null)
            return 1500;
        return -1;
    }

    public void update(LocationPack locationPack) {
        if (locationPack != null) {
            if (locationPack.getGpsLocation() != null)
                setGpsLocation(locationPack.getGpsLocation());
            if (locationPack.getCell() != null)
                setCell(locationPack.getCell());
            if (locationPack.getLocationName() != null)
                setLocationName(locationPack.getLocationName());
            if (locationPack.getWifiList() != null)
                setWifiList(locationPack.getWifiList());
        }
    }


    public enum ComparisonResult {Same, Near, Far}
}
