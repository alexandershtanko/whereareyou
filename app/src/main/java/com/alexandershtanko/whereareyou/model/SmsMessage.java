package com.alexandershtanko.whereareyou.model;

import com.orm.SugarRecord;

import java.util.Date;

/**
 * Created by aleksandr on 04.06.15.
 */
public class SmsMessage  extends SugarRecord<SmsMessage> {
    public MessageType messageType=MessageType.MESSAGE_RESPONSE;
    public String message = "";
    public String phone = "";
    public enum MessageType {MESSAGE_REQUEST,MESSAGE_RESPONSE};
    public Long dateAdd=(new Date()).getTime();

    public SmsMessage() {
    }

    public SmsMessage(String message, String phone,MessageType messageType) {
        this.message = message;
        this.phone = phone;
        this.messageType=messageType;
    }
}
