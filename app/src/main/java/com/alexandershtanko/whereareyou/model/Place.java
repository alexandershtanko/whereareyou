package com.alexandershtanko.whereareyou.model;

import java.io.Serializable;

/**
 * Created by aleksandr on 12.06.15.
 */
public class Place implements Serializable {
    //Не изменять!
    private String name;
    private LocationPack locationPack;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocationPack getLocationPack() {
        return locationPack;
    }

    public void setLocationPack(LocationPack locationPack) {
        this.locationPack = locationPack;
    }



}
