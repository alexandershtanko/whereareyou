package com.alexandershtanko.whereareyou.model;

import android.location.Location;

import java.io.Serializable;

/**
 * Created by alexander on 6/17/15.
 */
public class GpsLocation implements Serializable {
    //Не изменять!
    private double latitude;
    private double longitude;
    private double altitude;
    private float accuracy;

    public float getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(float accuracy) {
        this.accuracy = accuracy;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public float distanceTo(GpsLocation gpsLocation) {
        float[] results = new float[10];
        Location.distanceBetween(latitude, longitude, gpsLocation.latitude, gpsLocation.longitude, results);
        return results[0];
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }
}
