package com.alexandershtanko.whereareyou.exception;

/**
 * Created by alexander on 7/2/15.
 */
public class SmsException extends Exception{
    public SmsException(String msg,Exception e)
    {
        super(msg,e);
    }

    public SmsException(String msg)
    {
        super(msg);
    }
}
