package com.alexandershtanko.whereareyou;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.orm.SugarApp;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

/**
 * Created by alexander on 6/19/15.
 */
@ReportsCrashes(
        formUri = "http://alexandershtanko.com/whereareyou/crashes/report.php",
        mode = ReportingInteractionMode.SILENT,
        buildConfigClass = MyApp.class)

public class MyApp extends SugarApp {

    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    public void onCreate() {
        ACRA.init(this);
        analytics = GoogleAnalytics.getInstance(this);
        analytics.setLocalDispatchPeriod(1800);
//
        tracker = analytics.newTracker("UA-64305804-1");
        tracker.enableExceptionReporting(false);
//        tracker.enableAdvertisingIdCollection(true);
        tracker.enableAutoActivityTracking(true);
        tracker.enableAdvertisingIdCollection(true);
        super.onCreate();
    }
}
