package com.alexandershtanko.whereareyou.billing;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.alexandershtanko.whereareyou.util.billing.IabHelper;
import com.alexandershtanko.whereareyou.util.billing.IabResult;
import com.alexandershtanko.whereareyou.util.billing.Inventory;
import com.alexandershtanko.whereareyou.util.billing.Purchase;

/**
 * Created by alexander on 7/16/15.
 */
public class PurchaseHelper {
    private static final String TAG = "BillingHelper";
    private static int ON_ACTIVITY_RESULT_REQUEST_CODE = 10001;
    private final Context context;
    String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiTfMXonFZsm9DUtNjSWqgn9Kb2Qlkt+9jnYy/Y3VH3bsRn6YO3+XVn6WCQnSqx6UcNVobdA63Oq9jBQqCUVYwJ2ACSa8JarEwjTl0sRzWEhbyfzBYNtp/cVqvBvhRkWmRfWoJsF+n1laHehruk3dRnuc2BuhgS7v6mhqtgDuKqg57vmpIZ8EmxeQaP9CJv+lR+jCPhKKXsvxYaY5EaqXqtjDP/H0eLDOnnDqQ4Y+kmSb6bsFUaCj3r9FSyW/31vZM98Y4ab+vsKEgh+Mx/6QKtnfLIOvqbXOwSEybF/F2vSNhJaLhBteAQdp9uEcK45fuxwysUu8RBgObts901isVQIDAQAB";
    private IabHelper mHelper;

    public PurchaseHelper(Context context) {
        this.context = context;
    }


    public void init(final OnInitListener onInitListener) {
        mHelper = new IabHelper(context, base64EncodedPublicKey);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (result.isFailure()) {
                    onInitListener.onError(result.getMessage());
                    Log.d(TAG, "In-app Billing setup failed: " +
                            result);
                } else {
                    onInitListener.onSuccess();
                    Log.d(TAG, "In-app Billing is set up OK");
                }
            }
        });
    }

    public void release() {
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }

    public void purchaseItem(Activity activity, String sku, String token, final OnPurchaseFinishedListener onPurchaseFinishedListener) {
        IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
            @Override
            public void onIabPurchaseFinished(IabResult result, Purchase info) {
                if (result.isFailure())
                    onPurchaseFinishedListener.onError(info, result.getMessage());
                else
                    onPurchaseFinishedListener.onSuccess(info);

            }
        };

        mHelper.launchPurchaseFlow(activity, sku, ON_ACTIVITY_RESULT_REQUEST_CODE,
                mPurchaseFinishedListener, token);
    }

    public boolean handleActivityResult(int requestCode, int resultCode, Intent data) {
        return mHelper.handleActivityResult(ON_ACTIVITY_RESULT_REQUEST_CODE,
                resultCode, data);
    }


    public void consumeItem(final String sku, final OnConsumeResultListener onConsumeResultListener) {
        final IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
                new IabHelper.OnConsumeFinishedListener() {
                    public void onConsumeFinished(Purchase purchase,
                                                  IabResult result) {

                        if (result.isSuccess()) {
                            onConsumeResultListener.onSuccess(purchase);
                        } else {
                            onConsumeResultListener.onError(result.getMessage());
                        }
                    }
                };
        IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener
                = new IabHelper.QueryInventoryFinishedListener() {
            public void onQueryInventoryFinished(IabResult result,
                                                 Inventory inventory) {

                if (result.isFailure()) {
                    onConsumeResultListener.onError(result.getMessage());
                } else {
                    Purchase purchase = inventory.getPurchase(sku);
                    if (purchase == null)
                        onConsumeResultListener.onError("Unable to find purchase");
                    else
                        mHelper.consumeAsync(purchase,
                                mConsumeFinishedListener);
                }
            }
        };
        mHelper.queryInventoryAsync(mReceivedInventoryListener);
    }


    public interface OnInitListener {
        void onSuccess();

        void onError(String msg);
    }

    public interface OnPurchaseFinishedListener {
        void onSuccess(Purchase info);

        void onError(Purchase info, String msg);
    }

    public interface OnConsumeResultListener {
        void onSuccess(Purchase info);

        void onError(String msg);
    }
}
