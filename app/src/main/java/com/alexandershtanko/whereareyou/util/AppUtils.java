package com.alexandershtanko.whereareyou.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

/**
 * Created by alexander on 6/23/15.
 */
public class AppUtils {
    private static final String TAG = "AppUtils";

    public static String getAppVerion(Context context)
    {
        String version="999";
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(
                    context.getPackageName(), 0);
            version = info.versionName;
        }
        catch (Exception e)
        {
            Log.e(TAG,"",e);
        }
        return version;
    }
}
