package com.alexandershtanko.whereareyou.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

/**
 * Created by alexander on 6/26/15.
 */
public class DrawableUtils {
    public static Drawable getScaledDrawable(Context context, int res, int width, int height) {
        Drawable dr = context.getResources().getDrawable(res);
        Bitmap bitmap = ((BitmapDrawable) dr).getBitmap();
        return new BitmapDrawable(context.getResources(), Bitmap.createScaledBitmap(bitmap, width, height, true));
    }
}
