package com.alexandershtanko.whereareyou.util;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

import com.alexandershtanko.whereareyou.model.Contact;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexander on 6/16/15.
 */
public class ContactUtils {
    private static final String TAG = "ContactUtils";

    public static String getContactId(ContentResolver contentResolver, Intent intent) {
        String id=null;
        Cursor cursor = contentResolver.query(intent.getData(), null, null, null, null);
        while (cursor.moveToNext()) {
            id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
        }
        cursor.close();
        return id;
    }

    private static List<String> getEmailList(ContentResolver contentResolver, String contactId) {
        List<String> emailList = new ArrayList<>();
        Cursor emails = contentResolver.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + contactId, null, null);
        while (emails.moveToNext()) {
            String emailAddress = emails.getString(emails.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
            emailList.add(emailAddress);
        }
        emails.close();
        return emailList;
    }

    public static List<String> getPhones(ContentResolver contentResolver, String contactId) {
        List<String> phoneList = new ArrayList<String>();
        Cursor phones = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
        while (phones.moveToNext()) {
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            phoneList.add(phoneNumber);
        }
        phones.close();
        return phoneList;
    }

    private static Uri getPhotoUri(ContentResolver contentResolver, String contactId) {
        try {
            Cursor cur = contentResolver.query(
                    ContactsContract.Data.CONTENT_URI,
                    null,
                    ContactsContract.Data.CONTACT_ID + "=" + contactId + " AND "
                            + ContactsContract.Data.MIMETYPE + "='"
                            + ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE + "'", null,
                    null);
            if (cur != null) {
                if (!cur.moveToFirst()) {
                    return null;
                }
            } else {
                return null;
            }
        } catch (Exception e) {
            Log.e(TAG, "", e);
            return null;
        }
        Uri person = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long
                .parseLong(contactId));
        return Uri.withAppendedPath(person, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
    }
    public static Contact getContactByPhone(ContentResolver contentResolver, String phoneNumber) {

        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor cursor =
                contentResolver.query(
                        uri,
                        new String[] { ContactsContract.PhoneLookup._ID},
                        null,
                        null,
                        null);
        try {

            if (cursor != null) {
                while (cursor.moveToNext()) {
                    String id = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
                    return getContactById(contentResolver,id);
                }
            }
        }
        finally {
            cursor.close();
        }


        return null;
    }
    public static Contact getContactById(ContentResolver contentResolver, String id) {
        Contact contact = new Contact();
        contact.id=id;
        try {
            Cursor cursor = contentResolver.query(ContactsContract.Data.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id, null, null);
            while (cursor.moveToNext()) {
                contact.name = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME));
                String hasPhone = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                if (hasPhone.equalsIgnoreCase("1"))
                    hasPhone = "true";
                else
                    hasPhone = "false";

                if (Boolean.parseBoolean(hasPhone)) {
                    contact.phoneList.addAll(getPhones(contentResolver, contact.id));
                }
                contact.emailList.addAll(getEmailList(contentResolver, contact.id));

                if(contact.photoUri==null)
                    contact.photoUri = getPhotoUri(contentResolver, contact.id);
            }
            cursor.close();
        }
        catch (Exception e)
        {
            Log.e(TAG,"",e);
        }

        return contact;
    }
}
