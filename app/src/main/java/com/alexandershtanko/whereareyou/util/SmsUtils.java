package com.alexandershtanko.whereareyou.util;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;

import com.alexandershtanko.whereareyou.exception.SmsException;
import com.alexandershtanko.whereareyou.model.SmsMessage;

import java.util.ArrayList;

/**
 * Created by aleksandr on 02.06.15.
 */
public class SmsUtils {
    private static final String TAG = "SmsUtils";

    public static void sendSms(Context context, SmsMessage smsMessage) throws SmsException {
        try {
            SmsManager sms = SmsManager.getDefault();
            String sent = "android.telephony.SmsManager.STATUS_ON_ICC_SENT";
            PendingIntent piSent = PendingIntent.getBroadcast(context, 0,new Intent(sent), 0);
            ArrayList<String> msgStringArray = sms.divideMessage(smsMessage.message);
            ArrayList<PendingIntent> pendingIntents=new ArrayList<>();
            for(String ignored :msgStringArray)
            {
                pendingIntents.add(piSent);
            }

            sms.sendMultipartTextMessage(smsMessage.phone, null, msgStringArray, pendingIntents, null);

        }
        catch (Exception e)
        {
            throw new SmsException("Unable to send sms",e);
        }
    }

    public static SmsMessage getSms(Bundle bundle) throws SmsException {

        if (bundle != null) {
            Object[] pdus = (Object[]) bundle.get("pdus");
            if (pdus==null||pdus.length == 0) {
                throw new SmsException("Unable to get SMS (pdus.length=0)");
            }
            android.telephony.SmsMessage[] messages = new android.telephony.SmsMessage[pdus.length];
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < pdus.length; i++) {
                messages[i] = android.telephony.SmsMessage.createFromPdu((byte[]) pdus[i]);
                sb.append(messages[i].getMessageBody());
            }
            SmsMessage smsMessage=new SmsMessage();
            smsMessage.phone = messages[0].getOriginatingAddress();
            smsMessage.message= sb.toString();
            return smsMessage;
        }
        else throw new SmsException("Bundle is null");
    }

    public static void deleteSMS(Context context, SmsMessage smsMessage) {
        try {
            Uri uriSms = Uri.parse("content://sms/inbox");
            Cursor c = context.getContentResolver().query(
                    uriSms,
                    new String[] { "_id", "thread_id", "address", "person",
                            "date", "body" }, "read=0", null, null);

            if (c != null && c.moveToFirst()) {
                do {
                    long id = c.getLong(0);
                    long threadId = c.getLong(1);
                    String address = c.getString(2);
                    String body = c.getString(5);
                    String date = c.getString(3);
                    Log.e("log>>>",
                            "0>" + c.getString(0) + "1>" + c.getString(1)
                                    + "2>" + c.getString(2) + "<-1>"
                                    + c.getString(3) + "4>" + c.getString(4)
                                    + "5>" + c.getString(5));
                    Log.e("log>>>", "date" + c.getString(0));

                    if (smsMessage.message.equals(body) && address.equals(smsMessage.phone)) {
                        // mLogger.logInfo("Deleting SMS with id: " + threadId);
                        context.getContentResolver().delete(
                                Uri.parse("content://sms/" + id), "date=?",
                                new String[] { c.getString(4) });
                        Log.e("log>>>", "Delete success.........");
                    }
                } while (c.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG,"", e);
        }
    }

}
