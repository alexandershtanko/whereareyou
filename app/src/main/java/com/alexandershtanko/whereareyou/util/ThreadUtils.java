package com.alexandershtanko.whereareyou.util;

import android.util.Log;

/**
 * Created by alexander on 7/2/15.
 */
public class ThreadUtils {
    private static final String TAG = "ThreadUtils";

    public static void sleep(long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            Log.e(TAG, "", e);
        }
    }
}
