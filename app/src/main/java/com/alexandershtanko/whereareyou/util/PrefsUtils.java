package com.alexandershtanko.whereareyou.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by alexander on 5/12/15.
 */
public class PrefsUtils {

    private static final String TAG = "PrefsUtils";

    public static void putString(Context context, String name, String string) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString(name, string);
        editor.commit();
    }

    public static String getString(Context context, String name) {
        return getString(context, name, "");
    }

    public static String getString(Context context, String name, String defaultValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(name, defaultValue);
    }


    public static void putStringSet(Context context, String name, Set<String> stringSet) {
        try {
            int currentApiVersion = android.os.Build.VERSION.SDK_INT;
            if (currentApiVersion >= Build.VERSION_CODES.HONEYCOMB) {
                SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
                editor.putStringSet(name, stringSet);
                editor.commit();
            } else
                putObject(context, name, stringSet);
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
    }

    public static Set<String> getStringSet(Context context, String name) {
        try {
            int currentApiVersion = android.os.Build.VERSION.SDK_INT;
            if (currentApiVersion >= Build.VERSION_CODES.HONEYCOMB) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                return prefs.getStringSet(name, Collections.EMPTY_SET);
            } else
                return new HashSet((ArrayList<String>) getObjectList(context, name, String.class));
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
        return Collections.EMPTY_SET;
    }

    public static void putObject(Context context, String name, Object object) throws PreferenceException {


        try {
            ObjectMapper objectMapper = new ObjectMapper();
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
            editor.putString(name, objectMapper.writeValueAsString(object));
            editor.commit();
        } catch (Exception e) {
            throw new PreferenceException(e);
        }


    }

    public static Object getObjectList(Context context, String name, Class itemClass) throws PreferenceException {

        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            String json = prefs.getString(name, "");
            ObjectMapper objectMapper = new ObjectMapper();
            JavaType type = objectMapper.getTypeFactory().constructParametricType(ArrayList.class, itemClass);
            return objectMapper.readValue(json, type);
        } catch (Exception e) {
            throw new PreferenceException(e);
        }


    }

    public static void putBoolean(Context context, String name, Boolean bool) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putBoolean(name, bool);
        editor.commit();
    }

    public static Boolean getBoolean(Context context, String name, Boolean defValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean(name, defValue);
    }


    public static class PreferenceException extends Exception {
        public PreferenceException(Exception e) {
            super(e);
        }
    }
}
