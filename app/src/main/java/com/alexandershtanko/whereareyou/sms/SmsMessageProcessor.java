package com.alexandershtanko.whereareyou.sms;

import android.content.Context;

import com.alexandershtanko.whereareyou.exception.SmsException;
import com.alexandershtanko.whereareyou.location.LocationTracker;
import com.alexandershtanko.whereareyou.model.LocationPack;
import com.alexandershtanko.whereareyou.model.SmsMessage;
import com.alexandershtanko.whereareyou.sms.helper.AllowedPhoneHelper;
import com.alexandershtanko.whereareyou.ui.Settings;

/**
 * Created by aleksandr on 04.06.15.
 */
class SmsMessageProcessor {
    private static final String TAG = "SmsMessageProcessor";


    public static SmsMessage getAnswer(Context context, SmsMessage receivedSmsMessage) throws SmsException {

        if (receivedSmsMessage != null) {
            if (AllowedPhoneHelper.isPhoneAllowed(context, receivedSmsMessage.phone)) {
                String answer = process(context, receivedSmsMessage.message);
                if (answer != null) {
                    SmsMessage answerSmsMessage = new SmsMessage(answer, receivedSmsMessage.phone, SmsMessage.MessageType.MESSAGE_RESPONSE);
                    answerSmsMessage.phone = receivedSmsMessage.phone;
                    return answerSmsMessage;
                } else throw new SmsException("Empty answer");
            } else throw new SmsException("Phone is not allowed");
        } else
            throw new SmsException("Received sms is null");
    }


    private static String process(Context context, String message) throws SmsException {
        int smsType = SmsParser.getSmsType(message);
        switch (smsType) {
            case SmsParser.SMS_TYPE_LOCATION:
                long locationPeriod=Settings.getLocationPeriod(context);
                LocationPack locationPack= LocationTracker.getLocationPack(context,locationPeriod,TAG);
                return SmsGenerator.generateLocationMessage(context, locationPack);


            case SmsParser.SMS_TYPE_UNDEFINED:
                throw new SmsException("Undefined sms type");

        }
        return null;
    }
}
