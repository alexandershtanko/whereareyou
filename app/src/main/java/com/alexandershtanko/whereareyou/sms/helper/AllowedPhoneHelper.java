package com.alexandershtanko.whereareyou.sms.helper;

import android.content.Context;
import android.telephony.PhoneNumberUtils;

import com.alexandershtanko.whereareyou.exception.SmsException;
import com.alexandershtanko.whereareyou.ui.Settings;
import com.alexandershtanko.whereareyou.util.ContactUtils;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by alexander on 7/2/15.
 */
public class AllowedPhoneHelper {
    public static boolean isPhoneAllowed(Context context, String phone) throws SmsException {
        Set<String> allowedPhones = getAllowedPhones(context);
        for (String allowedPhone : allowedPhones) {
            if (comparePhones(phone, allowedPhone))
                return true;
        }
        return false;
    }

    public static boolean comparePhones(String phone, String allowedPhone) {
        return PhoneNumberUtils.compare(phone, allowedPhone) || allowedPhone != null && phone != null && phone.length() > 9 && allowedPhone.contains(phone.substring(phone.length() - 10));
    }

    public static Set<String> getAllowedPhones(Context context) throws SmsException {
        try {
            Set<String> contactIds = Settings.getContactIds(context);
            Set<String> phones = new HashSet<>();
            for (String id : contactIds) {
                phones.addAll(ContactUtils.getPhones(context.getContentResolver(), id));
            }
            return phones;
        } catch (Exception e) {
            throw new SmsException("Unable to get allowed phones");
        }
    }

}
