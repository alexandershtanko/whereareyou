package com.alexandershtanko.whereareyou.sms;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.alexandershtanko.whereareyou.exception.SmsException;
import com.alexandershtanko.whereareyou.model.SmsMessage;
import com.alexandershtanko.whereareyou.ui.Settings;
import com.alexandershtanko.whereareyou.ui.notification.MyNotificationManager;
import com.alexandershtanko.whereareyou.util.SmsUtils;

/**
 * Created by aleksandr on 31.10.15.
 */
public class SmsSenderService extends Service {
    public static final String SMS_BUNDLE = "sms_bundle_extra";
    private static final String TAG = "SmsSenderService";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.hasExtra(SMS_BUNDLE)) {
            (new AsyncSendResponse()).execute(intent);
        }

        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        MyNotificationManager.hideLocationNotification(getApplicationContext());
        Log.e(TAG, "DESTROY SmsSenderService");
    }

    private void processMessage(Context context, SmsMessage receivedSmsMessage) throws SmsException {

        SmsMessage answerSmsMessage = SmsMessageProcessor.getAnswer(context, receivedSmsMessage);
        SmsUtils.sendSms(context, answerSmsMessage);

        receivedSmsMessage.save();
        answerSmsMessage.save();

        if (Settings.isInterceptEnabled(context))
            SmsUtils.deleteSMS(context, answerSmsMessage);

    }


    class AsyncSendResponse extends AsyncTask<Intent, Void, Void> {
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            MyNotificationManager.hideLocationNotification(getApplicationContext());
        }

        @Override
        protected Void doInBackground(Intent... params) {
            Intent intent = null;
            synchronized (SmsSenderService.this) {
                startForeground(MyNotificationManager.LOCATION_NOTIFY_ID, MyNotificationManager.showLocationNotification(SmsSenderService.this, ""));

                try {
                    intent = params[0];
                    SmsMessage smsMessage = SmsUtils.getSms(intent.getBundleExtra(SMS_BUNDLE));
                    smsMessage.messageType = SmsMessage.MessageType.MESSAGE_REQUEST;
                    processMessage(getApplicationContext(), smsMessage);
                } catch (Exception e) {
                    Log.e(TAG, "", e);
                }
                stopForeground(true);
            }
            if (intent != null)
                WakefulBroadcastReceiver.completeWakefulIntent(intent);
            return null;
        }


    }
}
