package com.alexandershtanko.whereareyou.sms;


import android.content.Context;

import com.alexandershtanko.whereareyou.R;
import com.alexandershtanko.whereareyou.location.helper.LocationHelper;
import com.alexandershtanko.whereareyou.model.LocationPack;
import com.alexandershtanko.whereareyou.model.PlaceSearchResult;
import com.alexandershtanko.whereareyou.sms.helper.LocationAnswerHelper;

/**
 * Created by alexander on 6/9/15.
 */
public class SmsGenerator {

    private static final String TAG = "SmsGenerator";


    public static String generateLocationMessage(Context context, LocationPack locationPack) {

        String message = "";

        if (locationPack != null) {
            String msg;
            PlaceSearchResult result = LocationHelper.findPlaceByLocationPack(context, locationPack);

            if ((msg = LocationAnswerHelper.getPlaceLocationAnswer(context, locationPack, result)) != null) {
                message = msg;

                if (result.place != null && result.comparisonResult == LocationPack.ComparisonResult.Same)
                    locationPack.update(result.place.getLocationPack());
            }
            else {

                if ((msg = LocationAnswerHelper.getGoogleMapsLinkAnswer(context, locationPack)) != null)
                    message = message + ((message != "") ? "\n" : "") + msg;
            }
        }
        if (message == "")
            message = context.getString(R.string.i_do_not_know_where_i_am);

        return message;
    }

}
