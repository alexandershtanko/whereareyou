package com.alexandershtanko.whereareyou.sms;

/**
 * Created by alexander on 6/9/15.
 */
class SmsParser {
    public final static int SMS_TYPE_UNDEFINED = 0;
    public final static int SMS_TYPE_LOCATION = 1;

    public static int getSmsType(String message) {
        if(isLocationMessage(message))
            return SMS_TYPE_LOCATION;
        return SMS_TYPE_UNDEFINED;
    }

    private static boolean isLocationMessage(String message) {
        return message.toLowerCase().contains("где ты")||message.toLowerCase().contains("ты где")||
               message.toLowerCase().contains("where are you")||message.toLowerCase().contains("where you");
    }
}
