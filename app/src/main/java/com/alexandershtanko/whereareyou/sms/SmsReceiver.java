package com.alexandershtanko.whereareyou.sms;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.alexandershtanko.whereareyou.model.SmsMessage;
import com.alexandershtanko.whereareyou.ui.Settings;
import com.alexandershtanko.whereareyou.util.SmsUtils;

/**
 * Created by aleksandr on 02.06.15.
 */
public class SmsReceiver extends WakefulBroadcastReceiver {

    private static final String TAG = "SmsReceiver";
    private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (!Settings.getEnabled(context))
            return;
        try {
            if (intent.getAction().equals(SMS_RECEIVED)) {
                Intent serviceIntent=new Intent(context,SmsSenderService.class);
                serviceIntent.putExtra(SmsSenderService.SMS_BUNDLE,intent.getExtras());

                startWakefulService(context,serviceIntent);

                SmsMessage smsMessage = SmsUtils.getSms(intent.getExtras());
                if (SmsParser.getSmsType(smsMessage.message) == SmsParser.SMS_TYPE_LOCATION && Settings.isInterceptEnabled(context)) {
                    this.abortBroadcast();
                    Log.d(TAG, "Abort broadcast");
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
    }


}
