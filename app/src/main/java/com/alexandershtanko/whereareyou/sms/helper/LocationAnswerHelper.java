package com.alexandershtanko.whereareyou.sms.helper;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.alexandershtanko.whereareyou.R;
import com.alexandershtanko.whereareyou.model.Cell;
import com.alexandershtanko.whereareyou.model.LocationPack;
import com.alexandershtanko.whereareyou.model.PlaceSearchResult;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by alexander on 7/2/15.
 */
public class LocationAnswerHelper {
    private static final String TAG = "SmsAnswerHelper";

    public static String getGoogleMapsLinkAnswer(Context context, LocationPack locationPack) {
        String message;
        try {
            if (locationPack.getGpsLocation() == null && locationPack.getCell() == null)
                return null;

            message = context.getString(R.string.i_on_the_map) + ": ";
            if (locationPack.getGpsLocation() != null)
                message += getGoogleMapsLink(locationPack.getGpsLocation().getLatitude(), locationPack.getGpsLocation().getLongitude()) + " .";
            else if (locationPack.getCell().getPhoneType() == TelephonyManager.PHONE_TYPE_GSM)
                message += getGoogleMapsLink(context, locationPack.getCell()) + " .";
            else return null;

            String locationName = locationPack.getLocationName();
            if (locationName != null && !locationName.equals(""))
                message += "\n " + context.getString(R.string.address) + ": " + locationName;

            message += "\n " + getAccuracyMessage(context, locationPack);
            return message;
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
        return null;
    }

    public static String getGoogleMapsLink(Double lat, Double lng) {
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
        DecimalFormat df = (DecimalFormat) nf;
        String latS = df.format(lat);
        String lngS = df.format(lng);
        return "https://www.google.com/maps/place/" + latS + "+" + lngS + "/@" + latS + "," + lngS + ",15z";
    }

    public static String getGoogleMapsLink(Context context, Cell cell) {
        int operatorId = context.getResources().getConfiguration().mnc;
        int countryCode = context.getResources().getConfiguration().mcc;
        return "http://alexandershtanko.com/whereareyou/cell2gmaps.php?cellid=" + cell.getCid() + "&lac=" + cell.getLac() + "&operatorid=" + operatorId + "&countrycode=" + countryCode;
    }


    public static String getPlaceLocationAnswer(Context context, LocationPack locationPack, PlaceSearchResult result) {
        String message;
        if (locationPack == null)
            return null;


        if (result == null || result.comparisonResult == LocationPack.ComparisonResult.Far)
            return null;
        else {
            if (result.comparisonResult == LocationPack.ComparisonResult.Near)
                message = context.getString(R.string.i_near_from) + " " + result.place.getName();

            else {
                message = context.getString(R.string.i_on_the_place) + " " + result.place.getName();
            }
            return message;
        }
    }

    private static String getAccuracyMessage(Context context, LocationPack locationPack) {
        if (locationPack == null)
            return "";
        return context.getString(R.string.accuracy) + ": " + locationPack.getApproximateAccuracy() + " " + context.getString(R.string.meters_short);
    }
}
