package com.alexandershtanko.whereareyou.location.gsm;

import android.content.Context;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;

/**
 * Created by alexander on 6/9/15.
 */
public class GsmTracker {
    private final TelephonyManager telephonyManager;

    public GsmTracker(Context context) {
          telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

    }

    public CellLocation getCellLocation() {
        return telephonyManager.getCellLocation();
    }

    public int getPhoneType() {
        return telephonyManager.getPhoneType();
    }
}
