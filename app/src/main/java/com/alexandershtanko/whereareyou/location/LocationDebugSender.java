package com.alexandershtanko.whereareyou.location;


import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.alexandershtanko.whereareyou.model.LocationPack;
import com.alexandershtanko.whereareyou.model.Wifi;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by alexander on 6/9/15.
 */
public class LocationDebugSender {
    public static final String LOCATION_UPDATE = "Location Update";
    public static final String DEBUG_DATA_EXTRA = "debug data extra";

    private static final long SEND_DELAY = 1;
    private static final String TAG = "LocationDebugSender";
    private Context context;
    private ScheduledExecutorService ses;

    public LocationDebugSender(Context context) {
        this.context = context;
    }

    public void start() {
        ses = Executors.newSingleThreadScheduledExecutor();
        ses.scheduleWithFixedDelay(new SendTask(), 0, SEND_DELAY, TimeUnit.SECONDS);
    }

    public void stop() {
        ses.shutdown();
    }

    private class SendTask implements Runnable {
        @Override
        public void run() {
            try {
                LocationPack locationPack = null;  //LocationTracker.getLocationPack(context,);

                String output = "";
                if (locationPack.getGpsLocation() != null) {
                    output += "---------------GPS---------------\n";
                    output += "Latitude:" + locationPack.getGpsLocation().getLatitude() + "\n";
                    output += "Longitude:" + locationPack.getGpsLocation().getLongitude() + "\n";
                }
                if (locationPack.getCell() != null) {

                    if (locationPack.getCell().getPhoneType() == TelephonyManager.PHONE_TYPE_GSM) {
                        output += "---------------GSM---------------\n";
                        output += "CID:" + (locationPack.getCell()).getCid() + " ";
                        output += "LAC:" + (locationPack.getCell()).getLac() + "\n";

                    }
                    if (locationPack.getCell().getPhoneType() == TelephonyManager.PHONE_TYPE_CDMA) {
                        output += "---------------CDMA---------------\n";
                        output += "Base station id:" + (locationPack.getCell()).getBaseStationId() + "\n";

                    }

                }
                if (locationPack.getWifiList() != null) {
                    output += "---------------WIFI--------------\n";
                    for (Wifi wifi : locationPack.getWifiList()) {
                        output += "BSSID:" + wifi.BSSID + " ";
                        output += "SSID:" + wifi.SSID + "\n";
                    }
                }


                Intent intent = new Intent();
                intent.setAction(LOCATION_UPDATE);
                intent.putExtra(DEBUG_DATA_EXTRA, output);
                context.sendBroadcast(intent);
            } catch (Exception e) {
                Log.e(TAG, "", e);
            }

        }
    }
}
