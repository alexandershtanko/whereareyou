package com.alexandershtanko.whereareyou.location.wifi;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.util.Log;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by alexander on 6/9/15.
 */
public class WifiTracker {
    private static final long SCAN_DELAY = 1;
    private static final String TAG = "WifiTracker";
    private final WifiManager wifiManager;
    private final Context context;
    private ScheduledExecutorService ses;
    private boolean wifiState;

    public WifiTracker(Context context) {
        this.context = context;
        wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

    }

    public void start() {
        wifiState = getWifiState();
        if (!wifiState)
            enableWifi();

        ses = Executors.newSingleThreadScheduledExecutor();
        ses.scheduleWithFixedDelay(new ScanTask(), 0, SCAN_DELAY, TimeUnit.MINUTES);
    }

    public void stop() {
        ses.shutdown();
        if (!wifiState)
            disableWifi();
    }

    public List<ScanResult> getWifiList() {
        try {
            return wifiManager.getScanResults();
        }
        catch (Exception e)
        {
            Log.e(TAG,"",e);
        }
        return Collections.EMPTY_LIST;

    }

    private void enableWifi() {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        wifiManager.setWifiEnabled(true);

    }

    private void disableWifi() {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        wifiManager.setWifiEnabled(false);

    }

    private boolean getWifiState() {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        return wifiManager.isWifiEnabled();

    }


    private class ScanTask implements Runnable {
        @Override
        public void run() {
            wifiManager.startScan();
        }
    }
}
