package com.alexandershtanko.whereareyou.location;

import android.content.Context;
import android.util.Log;

import com.alexandershtanko.whereareyou.R;
import com.alexandershtanko.whereareyou.location.gps.GpsTracker;
import com.alexandershtanko.whereareyou.location.gsm.GsmTracker;
import com.alexandershtanko.whereareyou.location.helper.LocationHelper;
import com.alexandershtanko.whereareyou.location.wifi.WifiTracker;
import com.alexandershtanko.whereareyou.model.LocationPack;
import com.alexandershtanko.whereareyou.ui.Settings;
import com.alexandershtanko.whereareyou.ui.notification.MyNotificationManager;
import com.alexandershtanko.whereareyou.util.ThreadUtils;

import java.util.ArrayList;
import java.util.List;


public class LocationTracker {
    public static final int SEC_IN_MS = 1000;
    public static final long MIN_DISCOVERY_TIME = 2000;
    private static final String TAG = "LocationTracker";
    private static LocationTracker instance;
    private final GpsTracker gpsTracker;
    private final GsmTracker gsmTracker;
    private final WifiTracker wifiTracker;
    private final Context context;
    private final List<String> lockList = new ArrayList<>();


    private LocationTracker(Context context) {
        this.context = context;
        gpsTracker = new GpsTracker(context);
        gsmTracker = new GsmTracker(context);
        wifiTracker = new WifiTracker(context);
    }

    private static synchronized LocationTracker getInstance(Context context) {
        if (instance == null)
            instance = new LocationTracker(context);
        return instance;
    }

    public static LocationPack getLocationPack(Context context, long locationSearchPeriodInMs, String LockTag) {
        LocationTracker.getInstance(context).init(LockTag);

        long time = locationSearchPeriodInMs;
        while (time > 0) {
            if (!Settings.isInterceptEnabled(context))
                MyNotificationManager.showLocationNotification(context, "\n" + (time / SEC_IN_MS) + " " + context.getString(R.string.sec_lost));
            Log.e(TAG, "Location time lost:" + (time / SEC_IN_MS));
            ThreadUtils.sleep(SEC_IN_MS);
            time -= SEC_IN_MS;
        }

        LocationPack locationPack = LocationTracker.getInstance(context).populateLocationPack();

        LocationTracker.getInstance(context).release(LockTag);
        return locationPack;
    }

    public boolean isActive() {
        return !lockListIsEmpty();
    }

    private synchronized void init(String lockTag) {
        if (lockListIsEmpty()) {
            gpsTracker.start();
            wifiTracker.start();

            if (!Settings.isInterceptEnabled(context))
                MyNotificationManager.showLocationNotification(context, "");
        }

        addLockTag(lockTag);
    }

    private synchronized void release(String lockTag) {
        removeLockTag(lockTag);
        if (lockListIsEmpty()) {
            gpsTracker.stop();
            wifiTracker.stop();

            if (!Settings.isInterceptEnabled(context))
                MyNotificationManager.hideLocationNotification(context);
        }
    }

    private LocationPack populateLocationPack() {
        LocationPack locationPack = new LocationPack();
        locationPack.setLocation(gpsTracker.getLocation());
        locationPack.setCell(gsmTracker.getCellLocation(), gsmTracker.getPhoneType());
        locationPack.setWifiListFromScanResultList(wifiTracker.getWifiList());
        locationPack.setLocationName(LocationHelper.getLocationName(context, locationPack.getGpsLocation()));
        return locationPack;
    }


    private void addLockTag(String lockTag) {
        synchronized (lockList) {
            if (!lockList.contains(lockTag))
                lockList.add(lockTag);
        }
    }

    private void removeLockTag(String lockTag) {
        synchronized (lockList) {
            lockList.remove(lockTag);
        }
    }

    private boolean lockListIsEmpty() {
        synchronized (lockList) {
            return lockList.isEmpty();
        }
    }
}
