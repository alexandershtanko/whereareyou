package com.alexandershtanko.whereareyou.location.gps;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;

import com.alexandershtanko.whereareyou.util.GpsUtils;

import java.util.Date;

/**
 * Created by alexander on 6/9/15.
 */
public class GpsTracker implements LocationListener {
    public static final int UPDATE_PERIOD = 10000;
    public static final int MIN_DISTANCE = 0;
    private static final String TAG = "GpsTracker";
    private static final long ANDROID_LOCATION_AVAILABLE_PERIOD = 30 * 1000; //30 sec
    private final LocationManager locationManager;
    private final Context context;
    private boolean flgGpsEnabled = false;


    public GpsTracker(Context context) {
        this.context = context;
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }

    public void start() {
        flgGpsEnabled = GpsUtils.isGpsEnabled(context);
        if (!GpsUtils.canToogleGps(context))
            GpsUtils.turnGPSOn(context);

        requestLocation(LocationManager.GPS_PROVIDER);
        requestLocation(LocationManager.NETWORK_PROVIDER);
        requestLocation(LocationManager.PASSIVE_PROVIDER);


    }

    public void requestLocation(String gpsProvider) {
        try {
            locationManager.requestLocationUpdates(gpsProvider, UPDATE_PERIOD, MIN_DISTANCE, this, Looper.getMainLooper());
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
    }

    public void stop() {
        locationManager.removeUpdates(this);

        if (!GpsUtils.canToogleGps(context) && !flgGpsEnabled)
            GpsUtils.turnGPSOff(context);
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    public Location getLocation() {
        try {
            return GpsUtils.getLastBestLocation(locationManager, new Date().getTime() - ANDROID_LOCATION_AVAILABLE_PERIOD);
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
        return null;
    }

}
