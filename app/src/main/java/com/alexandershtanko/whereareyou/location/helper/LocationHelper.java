package com.alexandershtanko.whereareyou.location.helper;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import com.alexandershtanko.whereareyou.model.GpsLocation;
import com.alexandershtanko.whereareyou.model.LocationPack;
import com.alexandershtanko.whereareyou.model.Place;
import com.alexandershtanko.whereareyou.model.PlaceSearchResult;
import com.alexandershtanko.whereareyou.ui.Settings;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by alexander on 7/2/15.
 */
public class LocationHelper {
    private static final String TAG = "LocationHelper";

    public static String getLocationNameByCoords(Context context, double latitude, double longitude) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        String locationName = null;
        try {
            List<Address> listAddresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (null != listAddresses && listAddresses.size() > 0) {
                locationName = listAddresses.get(0).getAddressLine(0);
            }
        } catch (IOException e) {
            Log.e(TAG, "", e);
        }
        return locationName;
    }

    public static PlaceSearchResult findPlaceByLocationPack(Context context,LocationPack locationPack)
    {
        PlaceSearchResult result=new PlaceSearchResult();
        try {

            List<Place> places = Settings.getPlaces(context);

            for (Place place : places) {
                if (place.getLocationPack() != null) {
                    LocationPack.ComparisonResult res = place.getLocationPack().compare(locationPack);
                    if (res != LocationPack.ComparisonResult.Far && result.comparisonResult != LocationPack.ComparisonResult.Same) {
                        result.place = place;
                        result.comparisonResult = res;
                    }
                }
            }

        }
        catch (Exception e){
            Log.e(TAG,"",e);
        }
        return result;
    }

    public static String getLocationName(Context context, GpsLocation gpsLocation) {
        if (gpsLocation != null) {
            double lat=gpsLocation.getLatitude();
            double lng = gpsLocation.getLongitude();
            return getLocationNameByCoords(context,lat , lng);

        }
        else return "";

    }
}
